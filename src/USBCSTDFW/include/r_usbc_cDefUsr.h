/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
******************************************************************************
* File Name    : r_usbc_cDefUsr.h
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB User definition
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/
#ifndef __R_USBC_CDEFUSR_H__
#define __R_USBC_CDEFUSR_H__


/*****************************************************************************
Macro definitions (for Preprocessor)
******************************************************************************/

#define	USBC_FW_OS_PP				1
#define	USBC_FW_NONOS_PP			2

#define	USBC_OS_CRE_USE_PP			1	/* cre_* system call USE */
#define	USBC_OS_CRE_NOTUSE_PP		2	/* cre_* system call Not USE */

#define	USBC_HS_PP					1	/* Hi-Speed */
#define	USBC_FS_PP					2	/* Full-Speed */

#define	USBC_1PORT_PP				1	/* 1 port mode */
#define	USBC_2PORT_PP				2	/* 2 port mode */

#define	USBC_HOST_PP				1
#define	USBC_PERI_PP				2
#define	USBC_HOST_PERI_PP			3
#define	USBC_OTG_PP					4
#define	USBC_PERI0_PERI1_PP			17
#define	USBC_PERI0_HOST1_PP			18
#define	USBC_HOST0_PERI1_PP			19
#define	USBC_HOST0_HOST1_PP			20

/* Clock mode */
#define	USBC_CLK_NOT_STOP_PP		0
#define	USBC_CLK_XCKE_USE_PP		1
#define	USBC_CLK_PCUT_USE_PP		2

/* ATCKM mode  */
#define	USBC_ATCKM_NOT_USE_PP		0
#define	USBC_ATCKM_USE_PP			1

/* Sleep mode */
#define	USBC_LPSM_DISABLE_PP		0	/* Low-power sleep disable (SOC) */
#define	USBC_LPSM_ENABLE_PP			1	/* Low-power sleep enable (ASSP) */

/* Select H/W type */
#define	USBC_592IP_PP				2
#define	USBC_596IP_PP				6
#define	USBC_597IP_PP				7
#define	USBC_FSIP_PP				71

/* IP mode PIPE MAX */
#define	USBC_IP_PIPE_9_PP			9	/* PIPE=9 */
#define	USBC_IP_PIPE_7_PP			7	/* PIPE=7 */

/* IP mode DEVADD MAX */
#define	USBC_IP_DEVADD_A_PP			10	/* DEVADD_MAX=10 */
#define	USBC_IP_DEVADD_5_PP			5	/* DEVADD_MAX=5 */

/* Select PIPEBUF fix or variable */
#define	USBC_PIPEBUF_FIX_PP			1
#define	USBC_PIPEBUF_CHANGE_PP		2

/* Setup specification of PID=NAK */
#define	USBC_NONPERIODIC_PP			1	/* Use PID_NAK */
#define	USBC_PERIODIC_PP			2

/* Select target chip define */
#define	USBC_ASSP_PP				1
#define	USBC_RX600_PP				2

/* Select target system define */
#define	USBC_MS7727RP03_PP			1
#define	USBC_RX62NRSK_PP			2

/* DMA */
#define	USBC_DMA_NOT_USE_PP			0	/* CPU */
#define	USBC_DMA_BLOCK_ONCE_PP		1	/* DMA size == MXPS * 1 */
#define	USBC_DMA_BLOCK_CONT_PP		2	/* DMA size == MXPS * n */

/* Data Trans mode */
#define	USBC_TRANS_DMA_PP			1
#define	USBC_TRANS_DTC_PP			2

/* Default Bus size */
#define	USBC_BUSSIZE_16_PP			16
#define	USBC_BUSSIZE_32_PP			32

/* Low Power Mode */
#define	USBC_LPWR_NOT_USE_PP		0
#define	USBC_LPWR_USE_PP			1

/* BYTE ENDIAN */
#define	USBC_BYTE_LITTLE_PP			0
#define	USBC_BYTE_BIG_PP			1


/*****************************************************************************
Macro definitions (for User define)
******************************************************************************/

/* SPEED mode */
#define	USBC_HS_DISABLE			(uint16_t)0
#define	USBC_HS_ENABLE			(uint16_t)1

/* H/W function type */
#define	USBC_HOST				(uint16_t)1		/* Host mode */
#define	USBC_PERI				(uint16_t)2		/* Peripheral mode */
#define	USBC_HOST_PERI			(uint16_t)3		/* Host/Peri mode */
#define	USBC_OTG				(uint16_t)4		/* Otg mode */

/* H/W function type */
#define	USBC_BIT0				(uint16_t)0x0001
#define	USBC_BIT1				(uint16_t)0x0002
#define	USBC_BIT2				(uint16_t)0x0004
#define	USBC_BIT3				(uint16_t)0x0008
#define	USBC_BIT4				(uint16_t)0x0010
#define	USBC_BIT5				(uint16_t)0x0020
#define	USBC_BIT6				(uint16_t)0x0040
#define	USBC_BIT7				(uint16_t)0x0080
#define	USBC_BIT8				(uint16_t)0x0100
#define	USBC_BIT9				(uint16_t)0x0200
#define	USBC_BIT10				(uint16_t)0x0400
#define	USBC_BIT11				(uint16_t)0x0800
#define	USBC_BIT12				(uint16_t)0x1000
#define	USBC_BIT13				(uint16_t)0x2000
#define	USBC_BIT14				(uint16_t)0x4000
#define	USBC_BIT15				(uint16_t)0x8000
#define	USBC_BITSET(x)			(uint16_t)((uint16_t)1 << (x))

/* nonOS Use */
#define	USBC_SEQ_0				(uint16_t)0x0000
#define	USBC_SEQ_1				(uint16_t)0x0001
#define	USBC_SEQ_2				(uint16_t)0x0002
#define	USBC_SEQ_3				(uint16_t)0x0003
#define	USBC_SEQ_4				(uint16_t)0x0004
#define	USBC_SEQ_5				(uint16_t)0x0005
#define	USBC_SEQ_6				(uint16_t)0x0006
#define	USBC_SEQ_7				(uint16_t)0x0007
#define	USBC_SEQ_8				(uint16_t)0x0008
#define	USBC_SEQ_9				(uint16_t)0x0009
#define	USBC_SEQ_10				(uint16_t)0x000a

#define	USBC_HUB_P1				(uint16_t)0x0001
#define	USBC_HUB_P2				(uint16_t)0x0002
#define	USBC_HUB_P3				(uint16_t)0x0003
#define	USBC_HUB_P4				(uint16_t)0x0004


/*****************************************************************************
******************************************************************************/

/*****************************************************************************
Macro definitions (USER DEFINE)
******************************************************************************/

/* Select OS */
//	#define	USBC_FW_PP				USBC_FW_OS_PP
//	#define	USBC_FW_PP				USBC_FW_NONOS_PP

/* Select PORT */
	#define	USBC_PORTSEL_PP			USBC_1PORT_PP	/* 1port in 1IP */
//	#define	USBC_PORTSEL_PP			USBC_2PORT_PP	/* 2port in 1IP */

/* Select cre_* system call */
//	#define	USBC_OS_CRE_MODE_PP		USBC_OS_CRE_USE_PP
	#define	USBC_OS_CRE_MODE_PP		USBC_OS_CRE_NOTUSE_PP


/* Select target chip define */
//	#define	USBC_TARGET_CHIP_PP		USBC_ASSP_PP
	#define	USBC_TARGET_CHIP_PP		USBC_RX600_PP


/* Select IP */
//	#define	USBC_IPSEL_PP			USBC_592IP_PP
//	#define	USBC_IPSEL_PP			USBC_596IP_PP
	#define	USBC_IPSEL_PP			USBC_597IP_PP

/* Select Transfer Speed */
//	#define	USBC_SPEEDSEL_PP		USBC_HS_PP
	#define	USBC_SPEEDSEL_PP		USBC_FS_PP
//	#define	USBC_SPEEDSEL_PP		USBC_LS_PP

/* Select IP mode PIPE MAX */
//	#define	USBC_IP_PIPE_PP			USBC_IP_PIPE_9_PP	/* PIPE=9 */
	#define	USBC_IP_PIPE_PP			USBC_IP_PIPE_7_PP	/* PIPE=7 */

/* Select IP mode DEVADD MAX */
//	#define	USBC_IP_DEVADD_PP		USBC_IP_DEVADD_A_PP	/* DEVADD_MAX=10 */
	#define	USBC_IP_DEVADD_PP		USBC_IP_DEVADD_5_PP	/* DEVADD_MAX=5 */

/* Select PIPEBUF fix or variable */
	#define	USBC_PIPEBUF_MODE_PP	USBC_PIPEBUF_FIX_PP
//	#define	USBC_PIPEBUF_MODE_PP	USBC_PIPEBUF_CHANGE_PP

/* CPU byte endian select (BYTE_LITTLE:little, BYTE_BIG:big) */
	#define	USBC_CPUBYTE_PP			USBC_BYTE_LITTLE_PP
//	#define	USBC_CPUBYTE_PP			USBC_BYTE_BIG_PP

/* Default MBW */
	#define	USBC_BUSSIZE_PP			USBC_BUSSIZE_16_PP
//	#define	USBC_BUSSIZE_PP			USBC_BUSSIZE_32_PP


/* Setup specification of PID=NAK */
/* Whether a non-periodic pipe at the time of a transmission end */
	#define	USBC_PERIODIC_MODE_PP	USBC_NONPERIODIC_PP	/* Use PID_NAK */
//	#define	USBC_PERIODIC_MODE_PP	USBC_PERIODIC_PP

/* Data Trans mode */
	#define	USBC_TRANS_MODE_PP		USBC_TRANS_DTC_PP
//	#define	USBC_TRANS_MODE_PP		USBC_TRANS_DMA_PP

/* Low Power Mode */
	#define	USBC_LPWR_MODE_PP		USBC_LPWR_NOT_USE_PP
//	#define	USBC_LPWR_MODE_PP		USBC_LPWR_USE_PP


/*****************************************************************************
Macro definitions (User define)
******************************************************************************/

/* Interrupt message num */
#define	USBC_INTMSGMAX			(uint16_t)15
#define	USBC_DMAMSGMAX			(uint16_t)15

/* VIF select (VIF1:1.8V, VIF3:3.3V) */
//	#define	USBC_LDRVSEL		USBC_VIF1
	#define	USBC_LDRVSEL		USBC_VIF3

/* XIN define (XTAL48:48MHz, XTAL24:24MHz, XTAL12:12MHz) */
//	#define	USBC_XINSEL			USBC_XTAL12
	#define	USBC_XINSEL			USBC_XTAL24
//	#define	USBC_XINSEL			USBC_XTAL48


/*****************************************************************************
Macro definitions (User define HOST)
******************************************************************************/

/* Max pipe error count */
	#define	USBC_PIPEERROR		1u

/* Descriptor size */
	#define	USBC_DEVICESIZE		20u		/* Device Descriptor size */
	#define	USBC_CONFIGSIZE		256u	/* Configuration Descriptor size */

/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_596IP_PP
	/* USB Device address define */
	#define	USBC_DEVICEADDR		2u		/* USB Address (1 or 2 or 3) */
#endif	/* USBC_596IP_PP */

/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_597IP_PP
	/* USB Device address define */
	#define	USBC_DEVICEADDR		1u		/* PORT0 USB Address (1 to 10) */
	/* HUB down port */
	#define	USBC_HUBDOWNPORT	4u		/* HUB downport (MAX15) */
#endif	/* USBC_597IP_PP */


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_ASSP_PP
	#include	"r_usbc_cTypedef.h"
/* Condition compilation by the difference of IP */
  #if USBC_IPSEL_PP == USBC_597IP_PP
	#include	"r_usbc_cDef597IP.h"
    #include	"r_usbc_cUsbDefBitDefine.h"
  #endif	/* USBC_597IP_PP */
/* Condition compilation by the difference of IP */
  #if USBC_IPSEL_PP == USBC_596IP_PP
    #include	"r_usbc_cDef596IP.h"
    #include	"r_usbc_cUsbDefBitDefine.h"
  #endif	/* USBC_596IP_PP */
/* Condition compilation by the difference of IP */
  #if USBC_IPSEL_PP == USBC_592IP_PP
    #include	"r_usbc_cDef592IP.h"
    #include	"r_usbc_cUsbDefBitDefine.h"
  #endif	/* USBC_592IP_PP */

#endif /* USBC_TARGET_CHIP_PP == USBC_ASSP_PP */


#endif	/* __R_USBC_CDEFUSR_H__ */
/******************************************************************************
End  Of File
******************************************************************************/
