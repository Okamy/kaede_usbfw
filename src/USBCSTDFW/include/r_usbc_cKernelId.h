/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usbc_cKernelId.h
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : ID Number Definition Header File
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/
#ifndef __R_USBC_CKERNELID_H__
#define __R_USBC_CKERNELID_H__

/*****************************************************************************
Macro definitions
******************************************************************************/
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_OS_PP

/* Condition compilation by the difference of the device's operating system */
 #if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP

/* Semaphore ID define */
#define USB_HCD_SEM			1u
#define USB2_HCD_SEM		2u


/* Task ID define */
#define	USB_MAIN_TSK		1u		/* Main */
#define	USB_PSMP_TSK		2u		/* Peripheral Sample */
#define	USB_PCD_TSK			3u		/* Peripheral Control Driver */
#define	USB2_MAIN_TSK		4u		/* Main */
#define	USB2_HSMP_TSK		5u		/* Host Sample */
#define	USB2_HCD_TSK		6u		/* Host Control Driver */
#define	USB2_MGR_TSK		7u		/* Host Manager */
#define	USB2_HUB_TSK		8u		/* Hub */
#define	USBC_IDLE_TSK		9u		/* Idle */


/* Task stack size define */
#define	USBC_TSK_STK		0x800u	/* Stack size */


/* Task priority define */
#define	USB_MAIN_PRI		6u		/* Main */
#define	USB_PSMP_PRI		5u		/* Peripheral Sample */
#define	USB_PCD_PRI			2u		/* Peripheral Control Driver */
#define	USB2_MAIN_PRI		6u		/* Main */
#define	USB2_HSMP_PRI		5u		/* Host Sample */
#define	USB2_HCD_PRI		2u		/* Host Control Driver */
#define	USB2_MGR_PRI		4u		/* Host Manager */
#define	USB2_HUB_PRI		3u		/* Hub */
#define	USBC_IDLE_PRI		10u		/* Idle */


/* Mailbox ID define */
#define	USB_CLS_MBX			1u		/* Device class driver */
#define	USB_PSMP_MBX		2u		/* Peripheral Sample */
#define	USB_PCD_MBX			3u		/* Peripheral Control Driver */
#define	USB2_CLS_MBX		4u		/* Device class driver */
#define	USB2_HSMP_MBX		5u		/* Host Sample */
#define	USB2_HCD_MBX		6u		/* Host Control Driver */
#define	USB2_MGR_MBX		7u		/* Host Manager */
#define	USB2_HUB_MBX		8u		/* Hub */
#define	USBC_IDLE_MBX		9u		/* Idle */


/* Mail box priority define */
#define	USBC_MBX_PRI		1u		/* Priority */


/* Memorypool ID define */
#define	USB_CLS_MPL			1u		/* Device class driver */
#define	USB_PSMP_MPL		2u		/* Peripheral Sample */
#define	USB_PCD_MPL			3u		/* Peripheral Control Driver */
#define	USB2_CLS_MPL		4u		/* Device class driver */
#define	USB2_HSMP_MPL		5u		/* Host Sample */
#define	USB2_HCD_MPL		6u		/* Host Control Driver */
#define	USB2_MGR_MPL		7u		/* Host Manager */
#define	USB2_HUB_MPL		8u		/* Hub */
#define	USBC_IDLE_MPL		9u		/* Idle */


/* Memorypool block define */
#define	USBC_BLK_CNT		0x20u	/* Block count */
#define	USBC_BLK_SIZ		0x40u	/* Block size */

 #endif /* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP */

/* Condition compilation by the difference of the device's operating system */
 #if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_NOTUSE_PP

#include	"kernel_id.h"			/* Kernel ID definition */

 #endif /* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_NOTUSE_PP */


#else /* USBC_FW_PP == USBC_FW_OS_PP */

/* Scheduler use define */
#define	USBC_TBLCLR			0u				/* Table clear */
#define	USBC_CNTCLR			0u				/* Counter clear */
#define	USBC_FLGCLR			0u				/* Flag clear */
#define	USBC_FLGSET			1u				/* Flag set */
#define	USBC_IDCLR			0xFFu			/* Priority clear */

/* Task ID define */
#define	USBC_TID_0			0u				/* Task ID 0 */
#define	USBC_TID_1			1u				/* Task ID 1 */
#define	USBC_TID_2			2u				/* Task ID 2 */
#define	USBC_TID_3			3u				/* Task ID 3 */
#define	USBC_TID_4			4u				/* Task ID 4 */
#define	USBC_TID_5			5u				/* Task ID 5 */
#define	USBC_TID_6			6u				/* Task ID 6 */
#define	USBC_TID_7			7u				/* Task ID 7 */
#define	USBC_TID_8			8u				/* Task ID 8 */
#define	USBC_TID_9			9u				/* Task ID 9 */

/* Task priority define */
#define	USBC_PRI_0			0u				/* Priority 0 */
#define	USBC_PRI_1			1u				/* Priority 1 */
#define	USBC_PRI_2			2u				/* Priority 2 */
#define	USBC_PRI_3			3u				/* Priority 3 */
#define	USBC_PRI_4			4u				/* Priority 4 */
#define	USBC_PRI_5			5u				/* Priority 5 */
#define	USBC_PRI_6			6u				/* Priority 6 */

/* Please set with user system */
#define	USBC_IDMAX			8u				/* Maximum Task ID +1 */
#define	USBC_PRIMAX			8u				/* Maximum Priority number +1 */
#define	USBC_SNDMAX			10u				/* Maximum SND_MSG block */
#define	USBC_TABLEMAX		(USBC_SNDMAX + 10u)
											/* Maximum priority table */
#define	USBC_BLKMAX			10u				/* Maximum block */


/* Peripheral Control Driver Task */
#define	USB_PCD_TSK			USBC_TID_0		/* Task ID */
#define	USB_PCD_PRI			USBC_PRI_0		/* Priority */
#define	USB_PCD_MBX			USB_PCD_TSK		/* Mailbox ID */
#define	USB_PCD_MPL			USB_PCD_TSK		/* Memorypool ID */

/* Condition compilation by the difference of the devices */
#if	USBC_TARGET_CHIP_PP != USBC_ASSP_PP
/* Host Control Driver Task */
#define	USB2_HCD_TSK		USBC_TID_1		/* Task ID */
#define	USB2_HCD_PRI		USBC_PRI_0		/* Priority */
#define	USB2_HCD_MBX		USB2_HCD_TSK	/* Mailbox ID */
#define	USB2_HCD_MPL		USB2_HCD_TSK	/* Memorypool ID */

/* Host Manager Task */
#define	USB2_MGR_TSK		USBC_TID_2		/* Task ID */
#define	USB2_MGR_PRI		USBC_PRI_1		/* Priority */
#define	USB2_MGR_MBX		USB2_MGR_TSK	/* Mailbox ID */
#define	USB2_MGR_MPL		USB2_MGR_TSK	/* Memorypool ID */

/* Hub Task */
#define	USB2_HUB_TSK		USBC_TID_3		/* Task ID */
#define	USB2_HUB_PRI		USBC_PRI_2		/* Priority */
#define	USB2_HUB_MBX		USB2_HUB_TSK	/* Mailbox ID */
#define	USB2_HUB_MPL		USB2_HUB_TSK	/* Memorypool ID */

/* Host Sample Task */
#define	USB2_HMSC_TSK		USBC_TID_4			/* Task ID */
#define	USB2_HMSC_PRI		USBC_PRI_3			/* Priority */
#define	USB2_HMSC_MBX		USB2_HMSC_TSK		/* Mailbox ID */
#define	USB2_HMSC_MPL		USB2_HMSC_TSK		/* Memorypool ID */

/* Host Sample Task */
#define	USB2_HSTRG_TSK		USBC_TID_5			/* Task ID */
#define	USB2_HSTRG_PRI		USBC_PRI_3			/* Priority */
#define	USB2_HSTRG_MBX		USB2_HSTRG_TSK		/* Mailbox ID */
#define	USB2_HSTRG_MPL		USB2_HSTRG_TSK		/* Memorypool ID */

/* Host Sample Task */
#define	USB2_HMSCSMP_TSK	USBC_TID_6			/* Task ID */
#define	USB2_HMSCSMP_PRI	USBC_PRI_4			/* Priority */
#define	USB2_HMSCSMP_MBX	USB2_HMSCSMP_TSK	/* Mailbox ID */
#define	USB2_HMSCSMP_MPL	USB2_HMSCSMP_TSK	/* Memorypool ID */

/* Host Sample Task */
#define	USB2_HSMP_TSK		USBC_TID_4			/* Task ID */
#define	USB2_HSMP_PRI		USBC_PRI_3			/* Priority */
#define	USB2_HSMP_MBX		USB2_HSMP_TSK		/* Mailbox ID */
#define	USB2_HSMP_MPL		USB2_HSMP_TSK		/* Memorypool ID */

#else	/* USBC_ASSP_PP */

/* Host Control Driver Task */
#define	USB_HCD_TSK			USBC_TID_1			/* Task ID */
#define	USB_HCD_PRI			USBC_PRI_0			/* Priority */
#define	USB_HCD_MBX			USB_HCD_TSK			/* Mailbox ID */
#define	USB_HCD_MPL			USB_HCD_TSK			/* Memorypool ID */

/* Host Manager Task */
#define	USB_MGR_TSK			USBC_TID_2			/* Task ID */
#define	USB_MGR_PRI			USBC_PRI_1			/* Priority */
#define	USB_MGR_MBX			USB_MGR_TSK			/* Mailbox ID */
#define	USB_MGR_MPL			USB_MGR_TSK			/* Memorypool ID */

/* Hub Task */
#define	USB_HUB_TSK			USBC_TID_3			/* Task ID */
#define	USB_HUB_PRI			USBC_PRI_2			/* Priority */
#define	USB_HUB_MBX			USB_HUB_TSK			/* Mailbox ID */
#define	USB_HUB_MPL			USB_HUB_TSK			/* Memorypool ID */

/* Host Sample Task */
#define	USB_HMSCSMP_TSK		USBC_TID_6			/* Task ID */
#define	USB_HMSCSMP_PRI		USBC_PRI_4			/* Priority */
#define	USB_HMSCSMP_MBX		USB_HMSCSMP_TSK		/* Mailbox ID */
#define	USB_HMSCSMP_MPL		USB_HMSCSMP_TSK		/* Memorypool ID */

/* Host Sample Task */
#define	USB_HSMP_TSK		USBC_TID_4			/* Task ID */
#define	USB_HSMP_PRI		USBC_PRI_3			/* Priority */
#define	USB_HSMP_MBX		USB_HSMP_TSK		/* Mailbox ID */
#define	USB_HSMP_MPL		USB_HSMP_TSK		/* Memorypool ID */
#endif	/* != USBC_ASSP_PP*/

/* Peripheral Sample Task */
#define	USB_PSMP_TSK		USBC_TID_5		/* Task ID */
#define	USB_PSMP_PRI		USBC_PRI_1		/* Priority */
#define	USB_PSMP_MBX		USB_PSMP_TSK	/* Mailbox ID */
#define	USB_PSMP_MPL		USB_PSMP_TSK	/* Memorypool ID */

/* Idle Task */
#define	USBC_IDL_TSK		USBC_TID_6			/* Task ID */
#define	USBC_IDL_PRI		USBC_PRI_6			/* Priority */
#define	USBC_IDL_MBX		USBC_IDL_TSK		/* Mailbox ID */
#define	USBC_IDL_MPL		USBC_IDL_TSK		/* Memorypool ID */

/* Peri strage driver task ID */
#define	USB_PMSC_TSK		USBC_TID_1		/* Task ID */
#define	USB_PMSC_PRI		USBC_PRI_1		/* Task priority */
#define	USB_PMSC_MBX		USB_PMSC_TSK	/* MailBox ID */
#define	USB_PMSC_MPL		USB_PMSC_TSK	/* Memory pool ID */

/* Peri Medium(ATAPI SCSI etc.) driver task ID */
#define	USB_PFLSH_TSK		USBC_TID_2		/* Task ID */
#define	USB_PFLSH_PRI		USBC_PRI_2		/* Task priority */
#define	USB_PFLSH_MBX		USB_PFLSH_TSK	/* MailBox ID */
#define	USB_PFLSH_MPL		USB_PFLSH_TSK	/* Memory pool ID */

#endif /* USBC_FW_PP == USBC_FW_OS_PP */

#endif	/* __R_USBC_CKERNELID_H__ */
/******************************************************************************
End  Of File
******************************************************************************/
