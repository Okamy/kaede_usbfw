/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usbc_cScheduler.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : None
* H/W Platform : Independent
* Description  : USB Host and Peripheral scheduler code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 24.08.2010 0.92    Optimized function
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cKernelId.h"		/* Kernel ID definition */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usbc_cMacSystemcall.h"	/* uITRON system call macro */
#include "r_usb_cExtern.h"			/* uITRON system call macro */

/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _schedule
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP

/******************************************************************************
Private global variables and functions
******************************************************************************/
/* Priority Table */
static USBC_MSG_t*	usb_scstd_TableAdd[USBC_PRIMAX][USBC_TABLEMAX];
static uint8_t		usb_scstd_TableID[USBC_PRIMAX][USBC_TABLEMAX];
static uint8_t		usb_scstd_PriR[USBC_PRIMAX];
static uint8_t		usb_scstd_PriW[USBC_PRIMAX];
static uint8_t		usb_scstd_Pri[USBC_IDMAX];

/* Schedule Set Flag  */
static uint8_t		usb_scstd_ScheduleFlag;

/* Fixed-sized memory pools */
static USBC_UTR_t	usb_scstd_Block[USBC_BLKMAX];
static uint8_t		usb_scstd_BlkFlg[USBC_BLKMAX];

static	USBC_MSG_t*	usb_scstd_Add_use;
static uint8_t		usb_scstd_ID_use;

/* Wait MSG */
static USBC_MSG_t*	usb_scstd_WaitAdd[USBC_IDMAX];
static uint16_t		usb_scstd_WaitCounter[USBC_IDMAX];


/******************************************************************************
External variables and functions
******************************************************************************/
USBC_ER_t	usbc_cstd_RecMsg( uint8_t id, USBC_MSG_t** mess, USBC_TM_t tm );
USBC_ER_t	usbc_cstd_SndMsg( uint8_t id, USBC_MSG_t* mess );
USBC_ER_t	usbc_cstd_iSndMsg( uint8_t id, USBC_MSG_t* mess );
USBC_ER_t	usbc_cstd_WaiMsg( uint8_t id, USBC_MSG_t* mess, uint16_t times );
void		usbc_cstd_WaitScheduler(void);
USBC_ER_t	usbc_cstd_PgetBlk( uint8_t id, USBC_UTR_t** blk );
USBC_ER_t	usbc_cstd_RelBlk( uint8_t id,  USBC_UTR_t* blk );
void		usbc_cstd_ScheInit(void);
void		R_usbc_cstd_Scheduler(void);
void		R_usbc_cstd_SetTaskPri(uint8_t tasknum, uint8_t pri);
uint8_t		R_usbc_cstd_CheckSchedule(void);

extern void		usb_cstd_IntEnable(void);
extern void		usb_cstd_IntDisable(void);


/******************************************************************************
Renesas Scheduler API functions
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cstd_RecMsg
Description     : Receive Message
Argument        : uint8_t id               : ID Number
                : USBC_MSG_t** mess          : Message Pointer
                : USBC_TM_t tm               : Timeout Value
Return          : uint16_t                  : USBC_E_OK / USBC_E_ERROR
******************************************************************************/
USBC_ER_t usbc_cstd_RecMsg( uint8_t id, USBC_MSG_t** mess, USBC_TM_t tm )
{
	if(( id < USBC_IDMAX ) && ( usb_scstd_ID_use < USBC_IDMAX ))
	{
		if( id == usb_scstd_ID_use )
		{
			*mess = usb_scstd_Add_use;
			return	USBC_E_OK;
		}
	}	
	return	USBC_E_ERROR;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cstd_SndMsg
Description     : Send Message
Argument        : uint8_t id               : ID Number
                : USBC_MSG_t* mess           : Message Pointer
Return          : USBC_ER_t                  : USBC_E_OK / USBC_E_ERROR
******************************************************************************/
USBC_ER_t usbc_cstd_SndMsg( uint8_t id, USBC_MSG_t* mess )
{
	USBC_ER_t	status;

	/* USB interrupt disable */
	usb_cstd_IntDisable();
	status = usbc_cstd_iSndMsg(id,mess);
	/* USB interrupt enable */
	usb_cstd_IntEnable();
	return status;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cstd_iSndMsg
Description     : Send Message(for interrupt)
Argument        : uint8_t id               : ID Number
                : USBC_MSG_t* mess           : Message Pointer
Return          : USBC_ER_t                  : USBC_E_OK / USBC_E_ERROR
******************************************************************************/
USBC_ER_t usbc_cstd_iSndMsg( uint8_t id, USBC_MSG_t* mess )
{
	uint8_t	usb_pri;		/* Task Priority */
	uint8_t	usb_write;		/* Priority Table Writing pointer */

	if( id < USBC_IDMAX )
	{
		/* Read priority and table pointer */
		usb_pri		= usb_scstd_Pri[id];
		usb_write	= usb_scstd_PriW[usb_pri];
		if( usb_pri < USBC_PRIMAX )
		{
			/* Renewal write pointer */
			usb_write++;
			if( usb_write >= USBC_TABLEMAX )
			{
				usb_write = USBC_TBLCLR;
			}
			/* Check pointer */
			if( usb_write == usb_scstd_PriR[usb_pri])
			{
				return	USBC_E_ERROR;
			}
			/* Save message */
			/* Set priority table */
			usb_scstd_TableID[usb_pri][usb_write]	= id;
			usb_scstd_TableAdd[usb_pri][usb_write]	= mess;
			usb_scstd_PriW[usb_pri]					= usb_write;
			return	USBC_E_OK;
		}
	}
	USBC_PRINTF0("SND_MSG ERROR !!\n");
	return	USBC_E_ERROR;

}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cstd_PgetBlk
Description     : Get memory block
Argument        : uint8_t id                : ID Number
                : USBC_UTR_t** blk           : Memory block Pointer
Return          : USBC_ER_t                  : USBC_E_OK / USBC_E_ERROR
******************************************************************************/
USBC_ER_t usbc_cstd_PgetBlk( uint8_t id, USBC_UTR_t** blk )
{
	uint8_t	usb_s_pblk_c;

	if( id < USBC_IDMAX )
	{
		usb_s_pblk_c = USBC_CNTCLR;
		while(usb_s_pblk_c != USBC_BLKMAX)
		{
			if(usb_scstd_BlkFlg[usb_s_pblk_c] == USBC_FLGCLR)
			{
				/* Acquire fixed-size memory block */
				*blk	= &usb_scstd_Block[usb_s_pblk_c];
				usb_scstd_BlkFlg[usb_s_pblk_c]	= USBC_FLGSET;
				return	USBC_E_OK;
			}
			usb_s_pblk_c++;
		}
		/* Error of BLK Table Full !!  */
		USBC_PRINTF1("usb_scBlkFlg[%d][] Full !!\n",id);
	}
	return	USBC_E_ERROR;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cstd_RelBlk
Description     : Release memory block
Argument        : uint8_t id                : ID Number
                : USBC_UTR_t* blk            : Memory block Pointer
Return          : USBC_ER_t                  : USBC_E_OK / USBC_E_ERROR
******************************************************************************/
USBC_ER_t usbc_cstd_RelBlk( uint8_t id,  USBC_UTR_t* blk )
{
	uint16_t	usb_s_rblk_c;

	if( id < USBC_IDMAX )
	{
		usb_s_rblk_c = USBC_CNTCLR;
		while(usb_s_rblk_c != USBC_BLKMAX)
		{
			if(blk == &usb_scstd_Block[usb_s_rblk_c])
			{
				/* Release fixed-size memory block */
				usb_scstd_BlkFlg[usb_s_rblk_c] = USBC_FLGCLR;
				return	USBC_E_OK;
			}
			usb_s_rblk_c++;
		}
		/* Error of BLK Flag is not CLR !!  */
		USBC_PRINTF0("TskBlk NO CLR !!\n");
	}
	return	USBC_E_ERROR;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cstd_WaiMsg
Description     : Wait Message
Argument        : uint8_t  id              : ID Number
                : USBC_MSG_t *mess           : Message pointer
                : uint16_t  times           : Timeout Value
Return          : USBC_ER_t                  : USBC_E_OK / USBC_E_ERROR
******************************************************************************/
USBC_ER_t usbc_cstd_WaiMsg( uint8_t id, USBC_MSG_t* mess, uint16_t times )
{
	if( id < USBC_IDMAX )
	{
		if( usb_scstd_WaitCounter[id] == 0 )
		{
			usb_scstd_WaitAdd[id] = mess;
			usb_scstd_WaitCounter[id] = times;
			return	USBC_E_OK;
		}
	}
	/* Error !!  */
	USBC_PRINTF0("WAI_MSG ERROR !!\n");
	return	USBC_E_ERROR;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usbc_cstd_WaitScheduler
Description     : Wait request scheduling
Argument        : none
Return          : none
******************************************************************************/
void usbc_cstd_WaitScheduler(void)
{
	USBC_ER_t	err;
	uint8_t		id;

	for( id=0; id < USBC_IDMAX; id++ )
	{
		if(usb_scstd_WaitCounter[id] != 0)
		{
			usb_scstd_WaitCounter[id]--;
			if(usb_scstd_WaitCounter[id] == 0)
			{
				err	= usbc_cstd_SndMsg(id, usb_scstd_WaitAdd[id]);
				if( err != USBC_E_OK )
				{
					usb_scstd_WaitCounter[id]++;
				}
			}
		}
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usbc_cstd_ScheInit
Description     : Scheduler Initialize
Argument        : none
Return          : none
******************************************************************************/
void usbc_cstd_ScheInit(void)
{

	uint8_t	i;
	uint8_t	j;

	/* Initial Scheduler */
	usb_scstd_ID_use		= USBC_NONE;
	usb_scstd_ScheduleFlag	= USBC_NONE;

	/* Initialize  priority table pointer and priority table */
	for(i=0;i!=USBC_PRIMAX;i++)
	{
		usb_scstd_PriR[i]	= USBC_NONE;
		usb_scstd_PriW[i]	= USBC_NONE;
		for(j=0;j!=USBC_TABLEMAX;j++)
		{
			usb_scstd_TableID[i][j]	= USBC_IDMAX;
		}
	}

	/* Initialize block table */
	for(i=0;i!=USBC_BLKMAX;i++)
	{
		usb_scstd_BlkFlg[i]			= USBC_NONE;
	}

	/* Initialize priority */
	for(i=0;i!=USBC_IDMAX;i++)
	{
		usb_scstd_Pri[i]			= (uint8_t)USBC_IDCLR;
		usb_scstd_WaitAdd[i]		= (USBC_MSG_t*)USBC_NONE;
		usb_scstd_WaitCounter[i]	= USBC_NONE;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usbc_cstd_Scheduler
Description     : Host scheduler
Argument        : none
Return          : none
******************************************************************************/
void R_usbc_cstd_Scheduler(void)
{
	uint8_t	usb_pri;		/* Priority Counter */
	uint8_t	usb_read;		/* Priority Table Reading pointer */

	/* wait msg */
	usbc_cstd_WaitScheduler();

	/* Priority Table Reading */
	usb_pri = USBC_CNTCLR;
	while( usb_pri < USBC_PRIMAX )
	{
		usb_read	= usb_scstd_PriR[usb_pri];
		if( usb_read != usb_scstd_PriW[usb_pri] )
		{
			/* Pri Table Reading pointer increment */
			usb_read++;		
			if( usb_read >= USBC_TABLEMAX )
			{
				usb_read = USBC_TBLCLR;
			}
			/* Set practice message */
			usb_scstd_ID_use	= usb_scstd_TableID[usb_pri][usb_read];
			usb_scstd_Add_use	= usb_scstd_TableAdd[usb_pri][usb_read];
			usb_scstd_TableID[usb_pri][usb_read]	= USBC_IDMAX;
			usb_scstd_PriR[usb_pri]					= usb_read;
			usb_scstd_ScheduleFlag					= USBC_FLGSET;
			usb_pri = USBC_PRIMAX;
		}
		else
		{
			usb_pri++;
		}
	}
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : R_usbc_cstd_SetTaskPri
Description     : Set Task priority
Argument        : uint8_t tasknum          : Setting Task id
                : uint8_t pri              : Setting Task priority
Return          : none
******************************************************************************/
void R_usbc_cstd_SetTaskPri(uint8_t tasknum, uint8_t pri)
{
	if(tasknum < USBC_IDMAX)
	{
		if(pri < USBC_PRIMAX)
		{
			usb_scstd_Pri[tasknum]=pri;
		}
		else if(pri == (uint8_t)USBC_IDCLR)
		{
			usb_scstd_Pri[tasknum]=(uint8_t)USBC_IDCLR;
		}
		else
		{
		}
	}
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : R_usbc_cstd_CheckSchedule
Description     : Check schedule
Argument        : none
Return          : none
******************************************************************************/
uint8_t R_usbc_cstd_CheckSchedule(void)
{
	uint8_t flg;

	flg = usb_scstd_ScheduleFlag;
	usb_scstd_ScheduleFlag = USBC_FLGCLR;
	return flg;
}
/******************************************************************************
End of function
******************************************************************************/

#endif /* USBC_FW_PP == USBC_FW_OS_PP */

/******************************************************************************
End  Of File
******************************************************************************/
