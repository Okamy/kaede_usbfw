/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_data.h
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Host Sample Data Header
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/
#ifndef __R_USB_CDATA_H__
#define __R_USB_CDATA_H__

/******************************************************************************
Constant macro definitions
******************************************************************************/
#define	USBC_USEPIPE_ALL_PP			1
#define	USBC_USEPIPE_1267_PP		2

	#define	USBC_USEPIPE_MODE_PP	USBC_USEPIPE_1267_PP
//	#define	USBC_USEPIPE_MODE_PP	USBC_USEPIPE_ALL_PP


/* Table max */
#define	USB_TBL_MAX					10

/* Sample data transfer size */
#define	USB_SMPL_TRNSIZE1			512		/* PIPE1 */
#define USB_SMPL_TRNSIZE2			512		/* PIPE2 */
#define USB_SMPL_TRNSIZE3			512		/* PIPE3 */
#define USB_SMPL_TRNSIZE4			512		/* PIPE4 */
#define USB_SMPL_TRNSIZE5			512		/* PIPE5 */
#define USB_SMPL_TRNSIZE6			512		/* PIPE6 */
#define USB_SMPL_TRNSIZE7			512		/* PIPE7 */
#define USB_SMPL_TRNSIZE8			0		/* PIPE8 */
#define USB_SMPL_TRNSIZE9			0		/* PIPE9 */

/* Sample data transfer count */
#define USB_SMPL_TRNCNT1			512		/* PIPE1 */
#define USB_SMPL_TRNCNT2			512		/* PIPE2 */
#define USB_SMPL_TRNCNT3			512		/* PIPE3 */
#define USB_SMPL_TRNCNT4			512		/* PIPE4 */
#define USB_SMPL_TRNCNT5			512		/* PIPE5 */
#define USB_SMPL_TRNCNT6			512		/* PIPE6 */
#define USB_SMPL_TRNCNT7			512		/* PIPE7 */
#define USB_SMPL_TRNCNT8			0		/* PIPE8 */
#define USB_SMPL_TRNCNT9			0		/* PIPE9 */

/******************************************************************************
External variables and functions
******************************************************************************/
extern USBC_UTR_t	usb_gcstd_SmplTrnMsg[];
extern uint32_t		usb_gcstd_SmplTrnCnt[];
extern uint32_t		usb_gcstd_SmplTrnSize[];
extern uint8_t		*usb_gcstd_SmplTrnPtr[];

/* Sample data transfer data area */
extern uint8_t		usb_gcstd_SmplTrnData1[];
extern uint8_t		usb_gcstd_SmplTrnData2[];
extern uint8_t		usb_gcstd_SmplTrnData3[];
extern uint8_t		usb_gcstd_SmplTrnData4[];
extern uint8_t		usb_gcstd_SmplTrnData5[];
extern uint8_t		usb_gcstd_SmplTrnData6[];
extern uint8_t		usb_gcstd_SmplTrnData7[];
extern uint8_t		usb_gcstd_SmplTrnData8[];
extern uint8_t		usb_gcstd_SmplTrnData9[];

void	usb_cstd_ClrMbx(USBC_ID_t mbxid);

#endif	/* __R_USB_CDATA_H__ */
/******************************************************************************
End  Of File
******************************************************************************/
