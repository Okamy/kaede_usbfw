/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_ATAPImemory.c
* Version      : 1.00
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Host and Peripheral Interrupt code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 17.06.2011 1.10    Version 1.10 Release
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cDefUsr.h"		/* System definition */
#include "r_usb_cMSCdefine.h"		/* USB	Mass Storage Class Header */


#if	0	// Medialogic: このファイルのデータは使わない

/******************************************************************************
Section    <Section Difinition> , "Project Sections"
******************************************************************************/
#pragma section _usb_media


/******************************************************************************
Constant macro definitions
******************************************************************************/
#define	USBC_PMSC_MEDIATYPE				0xF8u	/* Fixed media */
//#define	USBC_PMSC_MEDIATYPE				0xF0u	/* Removable media */

#define	USBC_PMSC_SIGNATURE				0xAA55u

#define	USBC_PMSC_SECTSIZE				0x0200ul	/* 512byte / sector */
#define	USBC_PMSC_CLSTSIZE				0x01u
#define	USBC_PMSC_FATNUM				0x02u

/*
	If the number of data areas of clusters is smaller
	than that of value 4085(4096-11), it is FAT12.
	If the number of data areas of clusters is smaller
	than that of value 65525(65536-11), it is FAT16.
	Otherwise it is FAT32.
*/
#define	USBC_PMSC_TOTALSECT				USBC_PMSC_MEDIASIZE / USBC_PMSC_SECTSIZE

#if USBC_PMSC_TOTALSECT < 4096
		#define	USBC_PMSC_FATLENGTH		341ul	/* FAT12 */
#else
	#if USBC_PMSC_TOTALSECT < 65536
		#define	USBC_PMSC_FATLENGTH		256ul	/* FAT16 */
	#else
		#define	USBC_PMSC_FATLENGTH		128ul	/* FAT32 */
	#endif
#endif

#define	USBC_MEDIA_FATSIZE	(((USBC_PMSC_TOTALSECT-8)/USBC_PMSC_FATLENGTH)+1)

#define	USBC_PMSC_ROOTTOP	(((USBC_MEDIA_FATSIZE*USBC_PMSC_FATNUM+1)/8+1)*8)
#define	USBC_MEDIA_FATTOP	(USBC_PMSC_ROOTTOP-\
								(USBC_MEDIA_FATSIZE*USBC_PMSC_FATNUM))

//#define	USBC_MEDIA_ROOTSIZE				2ul
#define	USBC_MEDIA_ROOTSIZE				1ul


/******************************************************************************
External variables and functions
******************************************************************************/


/******************************************************************************
Private global variables and functions
******************************************************************************/
uint8_t usb_gpmsc_MediaArea[USBC_PMSC_SECTSIZE] =
{
/* Master boor recorder (FAT16) */
	/* 0x00:JMP code (PBR) */
	0xEB,0x3C,0x90,
	/* 0x03:OEM name */
	'M','S','D','O','S','5','.','0',
	/* 0x0B:Sector size */
	(uint8_t)(USBC_PMSC_SECTSIZE & 0xfful),
	((uint8_t)(USBC_PMSC_SECTSIZE >> 8) & 0xfful),
	/* 0x0D:Cluster size */
	USBC_PMSC_CLSTSIZE,
	/* 0x0E:reserved (FAT entry) */
#if 0	// DENO_PRG
	0x02,0x00,
#else
	(uint8_t)(USBC_MEDIA_FATTOP & 0xfful),
	((uint8_t)(USBC_MEDIA_FATTOP >> 8) & 0xfful),
#endif
	/* 0x10:FAT number */
	USBC_PMSC_FATNUM,
	/* 0x11:ROOT entry address */
	0x00, 0x02,
	/* 0x13:Total sector(16bit) */
	(uint8_t)(USBC_PMSC_TOTALSECT & 0xfful),
	((uint8_t)(USBC_PMSC_TOTALSECT >> 8) & 0xfful),
	/* 0x15:Media type */
	USBC_PMSC_MEDIATYPE,
	/* 0x16:FAT size */
	(uint8_t)(USBC_MEDIA_FATSIZE & 0xfful),
	((uint8_t)(USBC_MEDIA_FATSIZE >> 8) & 0xfful),
	/* 0x18:Track sector */
	0x00,0x00,
	/* 0x1A:Head number */
	0x00,0x00,
	/* 0x1C:Offset sector */
	0x00,0x00,0x00,0x00,
	/* 0x20:Total sector(32bit) */
	0x00,0x00,0x00,0x00,
	/* 0x24:Drive number */
	0x00,
	/* 0x25:reserved */
	0x00,
	/* 0x26:Boot signature */
	0x00,
	/* 0x27:Volume serial number */
	0x00,0x00,0x00,0x00,
	/* 0x2B:Volume label */
	'N','O',' ','N','A','M','E',' ',' ',' ',' ',
	/* 0x36:File system type */
#if USBC_PMSC_FATLENGTH == 341
	'F','A','T','1','2',' ',' ',' ',
#endif
#if USBC_PMSC_FATLENGTH == 256
	'F','A','T','1','6',' ',' ',' ',
#endif
	0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	/* 0x1FEB:signature */
	(uint8_t)(USBC_PMSC_SIGNATURE & 0xffu),
	((uint8_t)(USBC_PMSC_SIGNATURE >> 8) & 0xffu),
};

uint8_t usb_gpmsc_Table1[USBC_PMSC_SECTSIZE] =
{
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
};


// DEMO_PRG

uint8_t usb_gpmsc_RootDir_dummy[512*4] =
{
	0x00,
};

uint8_t usb_gpmsc_TableFat1[512] =
{
	0xF8,0xFF,0xFF,0xFF,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

};

uint8_t usb_gpmsc_TableFat2[512] =
{
	0xF8,0xFF,0xFF,0xFF,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
};


uint8_t usb_gpmsc_RootDir0[USBC_PMSC_SECTSIZE] =
{
	'G' ,'R' ,'-' ,'S' ,'A' ,'K' ,'U' ,'R' ,'A' ,0x20,0x20,0x08,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

	// ファイル名
#if 1
	0x43,0x65,0x00,0x2E,0x00,0x68,0x00,0x74,0x00,0x6D,0x00,0x0F,0x00,0x43,0x6C,0x00,
	0x00,0x00,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x00,0x00,0xFF,0xFF,0xFF,0xFF,
	0x02,0x73,0x00,0x20,0x00,0x50,0x00,0x72,0x00,0x6F,0x00,0x0F,0x00,0x43,0x6A,0x00,
	0x65,0x00,0x63,0x00,0x74,0x00,0x20,0x00,0x48,0x00,0x00,0x00,0x6F,0x00,0x6D,0x00,
	0x01,0x47,0x00,0x61,0x00,0x64,0x00,0x67,0x00,0x65,0x00,0x0F,0x00,0x43,0x74,0x00,
	0x20,0x00,0x52,0x00,0x65,0x00,0x6E,0x00,0x65,0x00,0x00,0x00,0x73,0x00,0x61,0x00,
	0x47,0x41,0x44,0x47,0x45,0x54,0x7E,0x31,0x48,0x54,0x4D,0x20,0x00,0x2D,0x8D,0x6C,
	0xAB,0x40,0xAB,0x40,0x00,0x00,0xC0,0x68,0xAB,0x40,0x03,0x00,0x6A,0x00,0x00,0x00,
#endif

	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
};

uint8_t usb_gpmsc_RootDir1[USBC_PMSC_SECTSIZE * 32ul] =
{
	0x00,
};

uint8_t usb_gpmsc_RootDir2[USBC_PMSC_SECTSIZE] =
{
	// ファイル実体
#if 1
	0x3C,0x61,0x20,0x68,0x72,0x65,0x66,0x3D,0x22,0x68,0x74,0x74,0x70,0x3A,0x2F,0x2F,
	0x74,0x6F,0x6F,0x6C,0x2D,0x73,0x75,0x70,0x70,0x6F,0x72,0x74,0x2E,0x72,0x65,0x6E,
	0x65,0x73,0x61,0x73,0x2E,0x63,0x6F,0x6D,0x2F,0x6A,0x70,0x6E,0x2F,0x74,0x6F,0x6F,
	0x6C,0x6E,0x65,0x77,0x73,0x2F,0x67,0x72,0x2F,0x69,0x6E,0x64,0x65,0x78,0x2E,0x68,
	0x74,0x6D,0x6C,0x22,0x3E,0x0D,0x0A,0x47,0x61,0x64,0x67,0x65,0x74,0x20,0x52,0x65,
	0x6E,0x65,0x73,0x61,0x73,0x20,0x50,0x72,0x6F,0x6A,0x65,0x63,0x74,0x20,0x48,0x6F,
	0x6D,0x65,0x0D,0x0A,0x3C,0x2F,0x61,0x3E,0x0D,0x0A,0x00,0x00,0x00,0x00,0x00,0x00,
#endif

	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
};




/******************************************************************************
Renesas Abstracted ATAPI Driver API functions
******************************************************************************/

#endif	// Medialogic: このファイルのデータは使わない


/******************************************************************************
End  Of File
******************************************************************************/
