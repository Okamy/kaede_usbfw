/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
******************************************************************************
* File Name    : r_usb_cDefUsr.h
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB User definition
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/
#ifndef __R_USB_CDEFUSR_H__
#define __R_USB_CDEFUSR_H__


/*****************************************************************************
Macro definitions (597IP)
******************************************************************************/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_597IP_PP
	/* H/W function type */
	//	#define	USB_FUNCSEL_PP		USBC_HOST_PP
	//	#define	USB_FUNCSEL_PP		USBC_PERI_PP
	//	#define	USB_FUNCSEL_PP		USBC_HOST_PERI_PP

/* Condition compilation by the difference of the devices */
  #if USBC_TARGET_CHIP_PP == USBC_ASSP_PP
	/* Clock mode */
		#define	USB_OSCSEL_PP		USBC_CLK_NOT_STOP_PP
	//	#define	USB_OSCSEL_PP		USBC_CLK_XCKE_USE_PP

	/* Sleep mode */
	/* If USBC_LPSM_ENABLE_PP is used,
		 sets USB_OSCSEL_PP = USBC_CLK_XCKE_USE_PP. */
		#define	USB_LPWRSEL_PP		USBC_LPSM_DISABLE_PP
	//	#define	USB_LPWRSEL_PP		USBC_LPSM_ENABLE_PP
	
		#define	USB_ATCKMSEL_PP		USBC_ATCKM_NOT_USE_PP
	//	#define	USB_ATCKMSEL_PP		USBC_ATCKM_USE_PP
  #else	/* !USBC_ASSP_PP */
	/* Clock mode */
		#define	USB_OSCSEL_PP		USBC_CLK_NOT_STOP_PP

	/* Sleep mode */
		#define	USB_LPWRSEL_PP		USBC_LPSM_DISABLE_PP

		#define	USB_ATCKMSEL_PP		USBC_ATCKM_NOT_USE_PP
  #endif	/* USBC_ASSP_PP */
#endif	/* USBC_597IP_PP */


/*****************************************************************************
Macro definitions (596IP)
******************************************************************************/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_596IP_PP
	/* H/W function type */
	//	#define	USB_FUNCSEL_PP		USBC_HOST_PP
	//	#define	USB_FUNCSEL_PP		USBC_PERI_PP
	//	#define	USB_FUNCSEL_PP		USBC_HOST_PERI_PP

	/* Select PORT */
		#define	USB_PORTSEL_PP		USBC_1PORT_PP

	/* Clock mode */
		#define	USB_OSCSEL_PP		USBC_CLK_NOT_STOP_PP
	//	#define	USB_OSCSEL_PP		USBC_CLK_XCKE_USE_PP
	//	#define	USB_OSCSEL_PP		USBC_CLK_PCUT_USE_PP

	/* ATCKM mode (In USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP *
	/* , USB_ATCKM_NOT_USE is prohibition. ) */
		#define	USB_ATCKMSEL_PP		USBC_ATCKM_NOT_USE_PP
	//	#define	USB_ATCKMSEL_PP		USBC_ATCKM_USE_PP
#endif	/* USBC_596IP_PP */


/*****************************************************************************
Macro definitions (592IP)
******************************************************************************/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_592IP_PP
	/* H/W function type */
		#define	USB_FUNCSEL_PP		USBC_PERI_PP

	/* Select PORT */
		#define	USB_PORTSEL_PP		USBC_1PORT_PP

	/* Clock mode */
		#define	USB_OSCSEL_PP		USBC_CLK_NOT_STOP_PP
	//	#define	USB_OSCSEL_PP		USBC_CLK_XCKE_USE_PP
	//	#define	USB_OSCSEL_PP		USBC_CLK_PCUT_USE_PP

	/* ATCKM mode (In USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP */
	/* , USB_ATCKM_NOT_USE is prohibition. ) */
		#define	USB_ATCKMSEL_PP		USBC_ATCKM_NOT_USE_PP
	//	#define	USB_ATCKMSEL_PP		USBC_ATCKM_USE_PP
#endif	/* USBC_592IP_PP */



/*****************************************************************************
Macro definitions (User define)
******************************************************************************/

/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_ASSP_PP
	/* USB register base address & pointer type */
	#define	USB_BASE			(uint32_t)(0x05000000)
#endif	/* USBC_ASSP_PP */

/* SPEED mode */
	#define	USB_HSESEL			USBC_HS_DISABLE
//	#define	USB_HSESEL			USBC_HS_ENABLE


/*****************************************************************************
Macro definitions (User define PERIPHERAL)
******************************************************************************/

	#define	USB_VENDORID		0x0000u		/* Vendor  ID */
	#define	USB_PRODUCTID		0x0000u		/* Product ID */

	#define	USB_BCDNUM			0x0200u		/* bcdUSB */
	#define	USB_RELEASE			0x0100u		/* Release Number */
	#define	USB_DCPMAXP			64u			/* DCP max packet size */
	#define	USB_CONFIGNUM		1u			/* Configuration number */
	#define	USB_STRINGNUM		7u			/* Max of string descriptor */


/*****************************************************************************
Macro definitions (User define HOST)
******************************************************************************/

/* Condition compilation by the difference of the devices */
#if USBC_PORTSEL_PP == USBC_1PORT_PP
	#define	USB_HUBDPADDR		(uint16_t)(USBC_DEVICEADDR + 1u)
#else	/* !USBC_1PORT_PP */
	#define	USB_HUBDPADDR		(uint16_t)(USBC_DEVICEADDR + 2u)
#endif	/* USBC_1PORT_PP */



/*****************************************************************************
Macro definitions
******************************************************************************/

/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_ASSP_PP
  /* Default MBW */
  #define	USB_CFIFO_MBW		USBC_MBW_16
  #define	USB_D0FIFO_MBW		USBC_MBW_16
  #define	USB_D1FIFO_MBW		USBC_MBW_16

  /* FIFO endian set (FIFO_LITTLE:little, FIFO_BIG:big) */
/* Condition compilation by the difference of the endian */
  #if USBC_CPUBYTE_PP == USBC_BYTE_LITTLE_PP
	#define USB_FIFOENDIAN		USBC_FIFO_LITTLE
	#define	USB_CFIFO_08		CFIFOREG.CFIFO08
	#define	USB_CFIFO_16		CFIFOREG.CFIFO16
	#define	USB_D0FIFO_08		D0FIFOREG.D0FIFO08
	#define	USB_D0FIFO_16		D0FIFOREG.D0FIFO16
	#define	USB_D1FIFO_08		D1FIFOREG.D1FIFO08
	#define	USB_D1FIFO_16		D1FIFOREG.D1FIFO16
	#define	USB_D0FIFO_08_ADR	(uint32_t)\
								(&usb_gcstd_UsbReg->D0FIFOREG.D0FIFO08)
	#define	USB_D0FIFO_16_ADR	(uint32_t)\
								(&usb_gcstd_UsbReg->D0FIFOREG.D0FIFO16)
	#define	USB_D1FIFO_08_ADR	(uint32_t)\
								(&usb_gcstd_UsbReg->D1FIFOREG.D1FIFO08)
	#define	USB_D1FIFO_16_ADR	(uint32_t)\
								(&usb_gcstd_UsbReg->D1FIFOREG.D1FIFO16)
  #else /* USBC_CPUBYTE_PP == USBC_BYTE_LITTLE_PP */
	#define USB_FIFOENDIAN		USBC_FIFO_BIG
	#define	USB_CFIFO_08		CFIFOREG.CFIFO08
	#define	USB_CFIFO_16		CFIFOREG.CFIFO16
	#define	USB_D0FIFO_08		D0FIFOREG.D0FIFO08
	#define	USB_D0FIFO_16		D0FIFOREG.D0FIFO16
	#define	USB_D1FIFO_08		D1FIFOREG.D1FIFO08
	#define	USB_D1FIFO_16		D1FIFOREG.D1FIFO16
	#define	USB_D0FIFO_08_ADR	(uint32_t)\
								(&usb_gcstd_UsbReg->D0FIFOREG.D0FIFO08)
	#define	USB_D0FIFO_16_ADR	(uint32_t)\
								(&usb_gcstd_UsbReg->D0FIFOREG.D0FIFO16)
	#define	USB_D1FIFO_08_ADR	(uint32_t)\
								(&usb_gcstd_UsbReg->D1FIFOREG.D1FIFO08)
	#define	USB_D1FIFO_16_ADR	(uint32_t)\
								(&usb_gcstd_UsbReg->D1FIFOREG.D1FIFO16)
  #endif /* USBC_CPUBYTE_PP == USBC_BYTE_LITTLE_PP */

#endif /* USBC_ASSP_PP */


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
    #include	"iodefine.h"
	#include	"r_usbc_cUsbDefBitDefine.h"
	#include	"r_usb_cMacUsr.h"

/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP

/*****************************************************************************
Macro definitions
******************************************************************************/

  /* USB register base address */
  #define	USB_IP				USB0

  /* Default MBW */
  #define	USB_CFIFO_MBW		USBC_MBW_16
  #define	USB_D0FIFO_MBW		USBC_MBW_16
  #define	USB_D1FIFO_MBW		USBC_MBW_16

  /* FIFO endian set (FIFO_LITTLE:little, FIFO_BIG:big) */
/* Condition compilation by the difference of the endian */
  #if USBC_CPUBYTE_PP == USBC_BYTE_LITTLE_PP
	#define USB_FIFOENDIAN		USBC_FIFO_LITTLE
	#define	USB_CFIFO_08		CFIFO.BYTE.L
	#define	USB_CFIFO_16		CFIFO.WORD
	#define	USB_D0FIFO_08		D0FIFO.BYTE.L
	#define	USB_D0FIFO_16		D0FIFO.WORD
	#define	USB_D1FIFO_08		D1FIFO.BYTE.L
	#define	USB_D1FIFO_16		D1FIFO.WORD
	#define	USB_D0FIFO_08_ADR	(uint32_t)(&USB_IP.D0FIFO.BYTE.L)
	#define	USB_D0FIFO_16_ADR	(uint32_t)(&USB_IP.D0FIFO.WORD)
	#define	USB_D1FIFO_08_ADR	(uint32_t)(&USB_IP.D1FIFO.BYTE.L)
	#define	USB_D1FIFO_16_ADR	(uint32_t)(&USB_IP.D1FIFO.WORD)
  #else /* USBC_CPUBYTE_PP == USBC_BYTE_LITTLE_PP */
	#define USB_FIFOENDIAN		USBC_FIFO_BIG
	#define	USB_CFIFO_08		CFIFO.BYTE.L
	#define	USB_CFIFO_16		CFIFO.WORD
	#define	USB_D0FIFO_08		D0FIFO.BYTE.L
	#define	USB_D0FIFO_16		D0FIFO.WORD
	#define	USB_D1FIFO_08		D1FIFO.BYTE.L
	#define	USB_D1FIFO_16		D1FIFO.WORD
	#define	USB_D0FIFO_08_ADR	(uint32_t)(&USB_IP.D0FIFO.BYTE.L)
	#define	USB_D0FIFO_16_ADR	(uint32_t)(&USB_IP.D0FIFO.WORD)
	#define	USB_D1FIFO_08_ADR	(uint32_t)(&USB_IP.D1FIFO.BYTE.L)
	#define	USB_D1FIFO_16_ADR	(uint32_t)(&USB_IP.D1FIFO.WORD)
  #endif /* USBC_CPUBYTE_PP == USBC_BYTE_LITTLE_PP */

#endif /* USBC_RX600_PP */


#endif	/* __R_USB_CDEFUSR_H__ */
/******************************************************************************
End  Of File
******************************************************************************/
