/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_pStdFunction.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Peripheral standard function code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usb_cExtern.h"			/* USB-FW global definition */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _pcd

/******************************************************************************
Renesas Abstracted Peripheral standard function functions
******************************************************************************/

/* r_usb_pStdRequest.c */
/******************************************************************************
Function Name   : usb_pstd_SetFeatureFunction
Description     : Receive SET_FEATURE request
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SetFeatureFunction(void)
{
	/* Request error */
	usb_cstd_SetStall((uint16_t)USBC_PIPE0);
}
/******************************************************************************
End of function
******************************************************************************/


/* r_usb_pDriver.c */
/******************************************************************************
Function Name   : usb_pstd_DtchFunction
Description     : Detach function
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_DtchFunction(void)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_IntHandFunction
Description     : Interrupt handler function
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_IntHandFunction(uint16_t ists1)
{
}
/******************************************************************************
End of function
******************************************************************************/


/* r_usb_pSignal.c */
/******************************************************************************
Function Name   : usb_pstd_ChkVbsts
Description     : Check VBUS status
Arguments       : none
Return          : uint16_t connection status(ATTACH/DETACH)
******************************************************************************/
uint16_t usb_pstd_ChkVbsts(void)
{
	uint16_t	buf1, buf2, buf3;
	uint16_t	connect_info;

	/* VBUS chattering cut */
	do
	{
		USB_RD(INTSTS0, buf1);
		usbc_cpu_Delay1us((uint16_t)10);
		USB_RD(INTSTS0, buf2);
		usbc_cpu_Delay1us((uint16_t)10);
		USB_RD(INTSTS0, buf3);
	}
	while( ((buf1 & USBC_VBSTS) != (buf2 & USBC_VBSTS))
		  || ((buf2 & USBC_VBSTS) != (buf3 & USBC_VBSTS)) );

	/* VBUS status judge */
	if( (buf1 & USBC_VBSTS) != (uint16_t)0 )
	{
		/* Attach */
		connect_info = USBC_ATTACH;
	}
	else
	{
		/* Detach */
		connect_info = USBC_DETACH;
	}
	return connect_info;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_pstd_AttachFunction
Description     : Attach detect processing function
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_AttachFunction(void)
{
	/* Delay about 10ms */
	usbc_cpu_DelayXms((uint16_t)10);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_DetachFunction
Description     : Detach detect processing function
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_DetachFunction(void)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_BusresetFunction
Description     : USB-reset detect processing function
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_BusresetFunction(void)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SuspendFunction
Description     : Suspend signal detect processing function
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SuspendFunction(void)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_InitFunction
Description     : Initialize function
Arguments       : none
Return value    : none
******************************************************************************/
uint16_t usb_pstd_InitFunction(void)
{
	/* Wait USBC_VBSTS */
	return usb_pstd_ChkVbsts();
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
End  Of File
******************************************************************************/
