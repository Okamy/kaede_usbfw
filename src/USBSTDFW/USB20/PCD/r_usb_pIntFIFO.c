/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_pIntFIFO.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Peripheral FIFO access code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usb_cExtern.h"			/* USB-FW global definition */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _pcd

/******************************************************************************
Renesas Abstracted Peripheral FIFO access functions
******************************************************************************/

/******************************************************************************
Function Name   : usb_pstd_BrdyPipe
Description     : INTR interrupt
Arguments       : uint16_t bitsts ; BRDYSTS Register & BRDYENB Register
Return value    : none
******************************************************************************/
void usb_pstd_BrdyPipe(uint16_t bitsts)
{
	/* Check PIPE */
	if( (bitsts & USBC_BRDY0) == USBC_BRDY0 )
	{
		switch( usb_cstd_Cfifo2Buf((uint16_t)USBC_PIPE0) )
		{
		/* End of data read */
		case USBC_READEND:
			/* Continue */
		/* End of data read */
		case USBC_READSHRT:
			usb_cstd_BrdyDisable((uint16_t)USBC_PIPE0);
			break;
		/* Continue of data read */
		case USBC_READING:
			/* PID = BUF */
			usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
			break;
		/* FIFO access error */
		case USBC_READOVER:
			USBC_PRINTF0("### Receive data over PIPE0 \n");
			/* Clear BVAL */
			USB_WR(CFIFOCTR, USBC_BCLR);
			/* Control transfer stop(end) */
			R_usb_pstd_ControlEnd((uint16_t)USBC_DATA_OVR);
			break;
		/* FIFO access error */
		case USBC_FIFOERROR:
			USBC_PRINTF0("### FIFO access error \n");
			/* Control transfer stop(end) */
			R_usb_pstd_ControlEnd((uint16_t)USBC_DATA_ERR);
			break;
		default:
			break;
		}
	}
	else
	{
		/* not PIPE0 */
		usb_cstd_BrdyPipe(bitsts);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_NrdyPipe
Description     : INTN interrupt
Arguments       : uint16_t bitsts ; NRDYSTS Register & NRDYENB Register
Return value    : none
******************************************************************************/
void usb_pstd_NrdyPipe(uint16_t bitsts)
{
	/* The function for peripheral driver is created here. */
	if( (bitsts & USBC_NRDY0) == USBC_NRDY0 )
	{
	}
	else
	{
		/* Nrdy Pipe interrupt */
		usb_cstd_NrdyPipe(bitsts);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_BempPipe
Description     : BEMP interrupt
Arguments       : uint16_t bitsts ; BEMPSTS Register & BEMPENB Register
Return value    : none
******************************************************************************/
void usb_pstd_BempPipe(uint16_t bitsts)
{
	if( (bitsts & USBC_BEMP0) == USBC_BEMP0 )
	{
		switch( usb_cstd_Buf2Cfifo((uint16_t)USBC_PIPE0) )
		{
		/* End of data write (not null) */
		case USBC_WRITEEND:
			/* Continue */
		/* End of data write */
		case USBC_WRITESHRT:
			/* Enable empty interrupt */
			usb_cstd_BempDisable((uint16_t)USBC_PIPE0);
			break;
		/* Continue of data write */
		case USBC_WRITING:
			/* PID = BUF */
			usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
			break;
		/* FIFO access error */
		case USBC_FIFOERROR:
			USBC_PRINTF0("### FIFO access error \n");
			/* Control transfer stop(end) */
			R_usb_pstd_ControlEnd((uint16_t)USBC_DATA_ERR);
			break;
		default:
			break;
		}
	}
	else
	{
		/* BEMP interrupt */
		usb_cstd_BempPipe(bitsts);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
End  Of File
******************************************************************************/
