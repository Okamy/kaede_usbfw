/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_pSignal.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Peripheral signal control code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usb_cExtern.h"			/* USB-FW global definition */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _pcd

/******************************************************************************
External variables and functions
******************************************************************************/
extern	uint16_t	usb_gpstd_intsts0;

/******************************************************************************
Renesas Abstracted Peripheral signal control functions
******************************************************************************/

/******************************************************************************
Function Name   : usb_pstd_BusReset
Description     : USB bus reset
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_BusReset(void)
{
	uint16_t	connect_info;

	/* Bus Reset */
	usb_pstd_BusresetFunction();

	/* Memory clear */
	usb_pstd_ClearMem();
	connect_info = usb_cstd_PortSpeed((uint16_t)USBC_PORT0);
	/* Callback */
	(*usb_gpstd_Driver.devdefault)(connect_info, (uint16_t)USBC_NO_ARG);
	/* DCP configuration register  (0x5C) */
	USB_WR(DCPCFG,	0);
	/* DCP maxpacket size register (0x5E) */
	USB_WR(DCPMAXP, usb_gpstd_Driver.devicetbl[USBC_DEV_MAX_PKT_SIZE]);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_remotewakeup
Description     : USB remotewakeup
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_RemoteWakeup(void)
{
	uint16_t	buf;

	/* Support remote wakeup ? */
	if( usb_gpstd_RemoteWakeup == USBC_YES )
	{
		/* RESM interrupt disable */
		USB_CLR_PAT(INTENB0, USBC_RSME);
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
#else	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
		/* External clock enable */
		USB_SET_PAT(SYSCFG0, USBC_XCKE);
#endif	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */

		/* RESM status read */
		USB_RD(INTSTS0, buf);
		if( (buf & USBC_RESM) != (uint16_t)0 )
		{
			/* RESM status clear */
			USB_CLR_STS(INTSTS0, USBC_RESM);
		}
		else
		{
			if( (buf & USBC_DS_SUSP) != (uint16_t)0 )
			{
				/* Remote wakeup set */
				USB_SET_PAT(DVSTCTR0, USBC_WKUP);
				usb_gpstd_intsts0	&= (uint16_t)(~USBC_DS_SUSP);
			}
		}
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_InitConnect
Description     : Check connection
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_InitConnect(void)
{
	uint16_t		connect_info;

	/* Interrupt enable */
	usb_pstd_InterruptEnable();
/* Condition compilation by the difference of user define */
#if USBC_SPEEDSEL_PP == USBC_HS_PP
	usb_cstd_SetHse((uint16_t)USBC_PORT0, usb_gcstd_HsEnable);
#endif	/* USBC_SPEEDSEL_PP == USBC_HS_PP */

	connect_info = usb_pstd_InitFunction();

	switch( connect_info )
	{
	/* Attach host controller */
	case USBC_ATTACH:
		usb_pstd_AttachProcess();
		break;
	/* Detach host controller */
	case USBC_DETACH:
		usb_pstd_DetachProcess();
		break;
	default:
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_pstd_TestMode
Description     : USB testmode (usb_pSetFeature)
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_TestMode(void)
{
/* Condition compilation by the difference of user define */
#if USBC_SPEEDSEL_PP == USBC_HS_PP
	switch( (uint16_t)(usb_gpstd_TestModeSelect & USBC_TEST_SELECT) )
	{
	case USBC_TEST_J:
		/* Continue */
	case USBC_TEST_K:
		/* Continue */
	case USBC_TEST_SE0_NAK:
		/* Continue */
	case USBC_TEST_PACKET:
		USB_CLR_PAT(TESTMODE, (uint16_t)USBC_UTST);
		USB_SET_PAT(TESTMODE, (uint16_t)(usb_gpstd_TestModeSelect >> 8));
		break;
	case USBC_TEST_FORCE_ENABLE:
		/* Continue */
	default:
		break;
	}
#endif	/* USBC_SPEEDSEL_PP == USBC_HS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/*""FUNC COMMENT""*************************************************************
Function Name   : usb_pstd_DpEnable
Description     : D+ Line Pull-up Enable
Arguments       : none
Return value    : none
*""FUNC COMMENT END""*********************************************************/
void usb_pstd_DpEnable(void)
{
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	USB_SET_PAT(SYSCFG, USBC_DPRPU);
#else	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
	USB_SET_PAT(SYSCFG0, USBC_DPRPU);
#endif	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/*""FUNC COMMENT""*************************************************************
Function Name   : usb_pstd_DpDisable
Description     : D+ Line Pull-up Disable
Arguments       : none
Return value    : none
*""FUNC COMMENT END""*********************************************************/
void usb_pstd_DpDisable(void)
{
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	USB_CLR_PAT(SYSCFG, USBC_DPRPU);
#else	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
	USB_CLR_PAT(SYSCFG0, USBC_DPRPU);
#endif	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/*""FUNC COMMENT""*************************************************************
Function Name   : usb_pstd_DmEnable
Description     : D- Line Pull-up Enable
Arguments       : none
Return value    : none
*""FUNC COMMENT END""*********************************************************/
void usb_pstd_DmEnable(void)
{
}
/******************************************************************************
End of function
******************************************************************************/


/*""FUNC COMMENT""*************************************************************
Function Name   : usb_pstd_DmDisable
Description     : D- Line Pull-up Disable
Arguments       : none
Return value    : none
*""FUNC COMMENT END""*********************************************************/
void usb_pstd_DmDisable(void)
{
}
/******************************************************************************
End of function
******************************************************************************/


/*===========================================================================*/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_597IP_PP

/******************************************************************************
Function Name   : usb_pstd_AttachProcess
Description     : USB attach
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_AttachProcess(void)
{
	usb_pstd_AttachFunction();
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	/* Pull-up enable */
	USB_SET_PAT(SYSCFG, USBC_DPRPU);
#else	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
	/* Pull-up enable */
	USB_SET_PAT(SYSCFG0, USBC_DPRPU);
#endif	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_DetachProcess
Description     : USB detach
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_DetachProcess(void)
{
	uint16_t		i, conf;
	uint16_t		*tbl;
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	/* Pull-up disable */
	USB_CLR_PAT(SYSCFG, USBC_DPRPU);
	usbc_cpu_Delay1us((uint16_t)2);
	USB_SET_PAT(SYSCFG, USBC_DCFM);
	usbc_cpu_Delay1us((uint16_t)1);
	USB_CLR_PAT(SYSCFG, USBC_DCFM);
#else	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
	/* Pull-up disable */
	USB_CLR_PAT(SYSCFG0, USBC_DPRPU);
	usbc_cpu_Delay1us((uint16_t)2);
	USB_SET_PAT(SYSCFG0, USBC_DCFM);
	usbc_cpu_Delay1us((uint16_t)1);
	USB_CLR_PAT(SYSCFG0, USBC_DCFM);
#endif	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */

	conf = usb_gpstd_ConfigNum;
	if( conf < (uint16_t)1 )
	{
		/* Address state */
		conf = (uint16_t)1;
	}

	/* Detach control */
	usb_pstd_DetachFunction();

	tbl	= usb_gpstd_Driver.pipetbl[conf - 1];
	for( i = 0; tbl[i] != USBC_PDTBLEND; i += USBC_EPL )
	{
		/* PID=BUF ? */
		if( usb_cstd_GetPid(tbl[i]) == USBC_PID_BUF )
		{
			usb_cstd_ForcedTermination(tbl[i], (uint16_t)USBC_DATA_STOP);
		}
		usb_cstd_ClrPipeCnfg(tbl[i]);
	}
	/* Callback */
	(*usb_gpstd_Driver.devdetach)((uint16_t)USBC_NO_ARG
		, (uint16_t)USBC_NO_ARG);
	usb_cstd_StopClock();
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SuspendProcess
Description     : USB suspend
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SuspendProcess(void)
{
	uint16_t	intsts0, buf;

	/* Resume interrupt enable */
	USB_SET_PAT(INTENB0, USBC_RSME);

	USB_RD(INTSTS0, intsts0);
	USB_RD(SYSSTS0,	 buf);
	if(((intsts0 & USBC_DS_SUSP) != (uint16_t)0) && ((buf & USBC_LNST) == USBC_FS_JSTS))
	{
		/* Suspend */
		usb_cstd_StopClock();
		usb_pstd_SuspendFunction();
	}
	/* --- SUSPEND -> RESUME --- */
	else
	{
		/* RESM status clear */
		USB_CLR_STS(INTSTS0, USBC_RESM);
		/* RESM interrupt disable */
		USB_CLR_PAT(INTENB0, USBC_RSME);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_ResumeProcess
Description     : USB resume
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_ResumeProcess(void)
{
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP
	uint16_t	buf;
#endif	/* USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP */

	/* RESM status clear */
	USB_CLR_STS(INTSTS0, USBC_RESM);
	/* RESM interrupt disable */
	USB_CLR_PAT(INTENB0, USBC_RSME);
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP
	USB_RD(INTSTS0, buf);
	/* --- USB bus reset ----- */
	if( (buf & USBC_DVSQ) == USBC_DS_DFLT )
	{
		/* Interrupt enable */
		usb_pstd_InterruptEnable();
		USB_RD(DVSTCTR0, buf);
		if( (buf & USBC_RHST) != USBC_HSPROC )
		{
			usb_pstd_BusReset();
		}
	}
#endif	/* USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP */
}
/******************************************************************************
End of function
******************************************************************************/
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
/*""FUNC COMMENT""*************************************************************
Function Name   : usb_pstd_RegRecover
Description     : Register Recover
Arguments       : none
Return value    : none
*""FUNC COMMENT END""*********************************************************/
void usb_pstd_RegRecover(void)
{
	void		usb_cpu_RegRecovEnable(void);
	void		usb_cpu_RegRecovDisable(void);
	uint16_t	buf;
	uint16_t*	table;

	if( (usb_gcstd_DvsqBit & USBC_DVSQS) != USBC_DS_POWR )
	{
		/* USB address */
		buf	 = usb_gcstd_AddrBit & USBC_USBADDR_MASK;
		/* USB device state */
		buf	|= ((usb_gcstd_DvsqBit & USBC_DVSQS) << 4);
		buf	|= USBC_STSR_SET;
		usb_cpu_RegRecovEnable();
		USB_WR(USBADDR, buf);
		usb_cpu_RegRecovDisable();
	}

	if( usb_gpstd_ConfigNum != 0 )
	{
		table = (uint16_t*)((uint16_t**)
			(usb_gpstd_Driver.pipetbl[usb_gpstd_ConfigNum-1]));
		/* Set pipe configuration register */
		R_usb_pstd_SetPipeRegister(USBC_PERIPIPE, table);
	}

	/* SQMON bit recover */
	if( (usb_gcstd_SqmonBit & USBC_BIT1) != 0 )
	{
		USB_WR(PIPE1CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT2) != 0 )
	{
		USB_WR(PIPE2CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT3) != 0 )
	{
		USB_WR(PIPE3CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT4) != 0 )
	{
		USB_WR(PIPE4CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT5) != 0 )
	{
		USB_WR(PIPE5CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT6) != 0 )
	{
		USB_WR(PIPE6CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT7) != 0 )
	{
		USB_WR(PIPE7CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT8) != 0 )
	{
		USB_WR(PIPE8CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT9) != 0 )
	{
		USB_WR(PIPE9CTR, USBC_SQSET);
	}
}
#endif /* USBC_TARGET_CHIP_PP == USBC_RX600_PP */

#endif	/* USBC_597IP_PP */
/******************************************************************************
End of function
******************************************************************************/



/*===========================================================================*/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_596IP_PP

/******************************************************************************
Function Name   : usb_pstd_ResumeProcess
Description     : USB resume
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_ResumeProcess(void)
{
	uint16_t	buf;
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP
	USB_RD(INTSTS0, buf);
	/* --- USB bus reset ----- */
	if( (buf & USBC_DVSQ) == USBC_DS_DFLT )
	{
		usb_cstd_AsspConfig();
		usb_cstd_Pinconfig();
		/* Interrupt enable */
		usb_pstd_InterruptEnable();
		USB_RD(DVSTCTR0, buf);
		if( (buf & USBC_RHST) != USBC_HSPROC )
		{
			/* Bus Reset */
			usb_pstd_BusReset();
		}
	}
#endif	/* USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP */
	usb_cstd_InterruptClock();
	usb_cstd_ResmClearSts();
	/* RESM interrupt disable */
	USB_CLR_PAT(INTENB0, USBC_RSME);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SuspendClock
Description     : Stop Clock for suspended
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SuspendClock(void)
{
	uint16_t	intsts0, buf;
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_NOT_STOP_PP
	/* nothing */
#endif	/* USB_OSCSEL_PP == USBC_CLK_NOT_STOP_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP
	usb_gcstd_PcutMode = USBC_PERI;
	usb_cstd_StopClock();
	/* Register backup */
	usb_cstd_RegBackup();
	/* PCUT enable */
	USB_SET_PAT(SYSCFG0, USBC_PCUT);
#endif	/* USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP
	usb_gcstd_XckeMode = USBC_YES;
	usb_cstd_StopClock();
	/* External clock disable */
	USB_CLR_PAT(SYSCFG0, USBC_XCKE);
/* Condition compilation by the difference of the devices */
#if USB_ATCKMSEL_PP == USBC_ATCKM_USE_PP
		USB_RD(SYSCFG0, buf);
		/* --- SUSPEND -> RESUME --- */
		if( (buf & USBC_XCKE) == USBC_XCKE )
		{
			usb_cstd_RestartClock();
			/* RESM status clear */
			usb_cstd_ResmClearSts();
			/* RESM interrupt disable */
			USB_CLR_PAT(INTENB0, USBC_RSME);
		}
#endif	/* USB_ATCKMSEL_PP == USBC_ATCKM_USE_PP */
#endif	/* USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_DetachClock
Description     : Stop Clock for detached
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_DetachClock(void)
{
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_NOT_STOP_PP
	usb_cstd_SwReset();
	usb_cstd_Pinconfig();
	/* Interrupt enable */
	usb_pstd_InterruptEnable();
#endif	/* USB_OSCSEL_PP == USBC_CLK_NOT_STOP_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP
	usb_gcstd_PcutMode = USBC_PERI;
	usb_cstd_StopClock();
	/* PCUT enable */
	USB_SET_PAT(SYSCFG0, USBC_PCUT);
#endif	/* USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP
	usb_gcstd_XckeMode = USBC_YES;
	usb_cstd_StopClock();
	/* External clock disable */
	USB_CLR_PAT(SYSCFG0, USBC_XCKE);
	usb_cstd_SwReset();
	usb_cstd_Pinconfig();
	/* Interrupt enable */
	usb_pstd_InterruptEnable();
#endif	/* USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP */
}

#endif	/* == USBC_596IP_PP */
/******************************************************************************
End of function
******************************************************************************/



/*===========================================================================*/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_592IP_PP

/******************************************************************************
Function Name   : usb_pstd_ResumeProcess
Description     : USB resume
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_ResumeProcess(void)
{
	uint16_t	buf;
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP
	USB_RD(INTSTS0, buf);
	/* --- USB bus reset ----- */
	if( (buf & USBC_DVSQ) == USBC_DS_DFLT )
	{
		usb_cstd_AsspConfig();
		usb_cstd_Pinconfig();
		/* Interrupt enable */
		usb_pstd_InterruptEnable();
		USB_RD(DVSTCTR0, buf);
		if( (buf & USBC_RHST) != USBC_HSPROC )
		{
			/* Bus Reset */
			usb_pstd_BusReset();
		}
	}
#endif	/* USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP */
	usb_cstd_InterruptClock();
	usb_cstd_ResmClearSts();
	/* RESM interrupt disable */
	USB_CLR_PAT(INTENB0, USBC_RSME);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SuspendClock
Description     : Stop Clock for suspended
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SuspendClock(void)
{
	uint16_t	intsts0, buf;
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_NOT_STOP_PP
	/* nothing */
#endif	/* USB_OSCSEL_PP == USBC_CLK_NOT_STOP_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP
	usb_gcstd_PcutMode = USBC_PERI;
	usb_cstd_StopClock();
	/* Register backup */
	usb_cstd_RegBackup();
	/* USBC_PCUT enable */
	USB_SET_PAT(SYSCFG0, USBC_PCUT);
#endif	/* USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP
	usb_gcstd_XckeMode = USBC_YES;
	usb_cstd_StopClock();
	/* External clock disable */
	USB_CLR_PAT(SYSCFG0, USBC_XCKE);
/* Condition compilation by the difference of the devices */
#if USB_ATCKMSEL_PP == USBC_ATCKM_USE_PP
		USB_RD(SYSCFG0, buf);
		/* --- SUSPEND -> RESUME --- */
		if( (buf & USBC_XCKE) == USBC_XCKE )
		{
			usb_cstd_RestartClock();
			/* RESM status clear */
			usb_cstd_ResmClearSts();
			/* RESM interrupt disable */
			USB_CLR_PAT(INTENB0, USBC_RSME);
		}
#endif	/* USB_ATCKMSEL_PP == USBC_ATCKM_USE_PP */
#endif	/* USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_DetachClock
Description     : Stop Clock for detached
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_DetachClock(void)
{
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_NOT_STOP_PP
	usb_cstd_SwReset();
	usb_cstd_Pinconfig();
	/* Interrupt enable */
	usb_pstd_InterruptEnable();
#endif	/* USB_OSCSEL_PP == USBC_CLK_NOT_STOP_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP
	usb_gcstd_PcutMode = USBC_PERI;
	usb_cstd_StopClock();
	/* PCUT enable */
	USB_SET_PAT(SYSCFG0, USBC_PCUT);
#endif	/* USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP
	usb_gcstd_XckeMode = USBC_YES;
	usb_cstd_StopClock();
	/* External clock disable */
	USB_CLR_PAT(SYSCFG0, USBC_XCKE);
	usb_cstd_SwReset();
	usb_cstd_Pinconfig();
	/* Interrupt enable */
	usb_pstd_InterruptEnable();
#endif	/* USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP */
}

#endif	/* == USBC_592IP_PP */
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
End  Of File
******************************************************************************/
