/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_cIntHandler.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Host and Peripheral Interrupt handler code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cKernelId.h"		/* Kernel ID definition */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usbc_cMacSystemcall.h"	/* System call macro */
#include "r_usb_cExtern.h"			/* USB-FW global define */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _usblib


/******************************************************************************
Constant macro definitions
******************************************************************************/


/******************************************************************************
External variables and functions
******************************************************************************/


/******************************************************************************
Private global variables and functions
******************************************************************************/
/* Interrupt message */
USBC_UTR_t		usb_gcstd_IntMsg[USBC_INTMSGMAX];
/* Interrupt message count */
uint16_t		usb_gcstd_IntMsgCnt = 0;

/* Interrupt message */
USBC_UTR_t		usb_gcstd_DmaMsg[USBC_INTMSGMAX];
/* Interrupt message count */
uint16_t		usb_gcstd_DmaMsgCnt = 0;

USBC_UTR_t		usb_gcstd_IntMsgD0fifo;


/******************************************************************************
Renesas Abstracted common Interrupt handler functions
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_InitUsbMessage
Description     : USB interrupt message initialize
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_InitUsbMessage(uint16_t function)
{
	/* TD.5.4 The interruption message of PCD is notified to HCD.  */
	uint16_t		i, msg_info;

	if( function == USBC_PERI )
	{
		/* Peripheral Function */
		msg_info = USBC_MSG_PCD_INT;
	}
	else
	{
		/* Host Function */
		msg_info = USBC_MSG_HCD_INT;
	}

	for( i = 0; i < USBC_INTMSGMAX; i++ )
	{
		usb_gcstd_IntMsg[i].msginfo = msg_info;
	}

	usb_gcstd_IntMsgCnt++;
	if( usb_gcstd_IntMsgCnt == USBC_INTMSGMAX )
	{
		usb_gcstd_IntMsgCnt = 0;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_DmaHandler
Description     : DMA interrupt Handler
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_DmaHandler(void)
{
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP || USBC_TARGET_CHIP_PP == USBC_ASSP_PP
	USBC_ER_t		err;
	USBC_UTR_t		*ptr;

	ptr = &usb_gcstd_IntMsgD0fifo;
	ptr->msghead = (USBC_MH_t)USBC_NULL;
	ptr->keyword = 0;
	ptr->status  = 0;

/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
	ptr->msginfo = USBC_MSG_PCD_D0FIFO_INT;
	/* Send message */
	err = USBC_ISND_MSG(USB_PCD_MBX, (USBC_MSG_t*)ptr);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("### DmaHandler isnd_msg error (%ld)\n", err);
	}
#endif	/* USB_FUNCSEL_PP == USBC_PERI_PP */

/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PP
	ptr->msginfo = USBC_MSG_HCD_D0FIFO_INT;
	/* Send message */
	err = USBC_ISND_MSG(USB_HCD_MBX, (USBC_MSG_t*)ptr);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("### DmaHandler isnd_msg error (%ld)\n", err);
	}
#endif	/* USB_FUNCSEL_PP == USBC_HOST_PP */

/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PERI_PP
	if( usb_cstd_FunctionUsbip() == USBC_NO )
	{
		/* Peripheral Function */
		ptr->msginfo = USBC_MSG_PCD_D0FIFO_INT;
		/* Send message */
		err = USBC_ISND_MSG(USB_PCD_MBX, (USBC_MSG_t*)ptr);
		if( err != USBC_E_OK )
		{
			USBC_PRINTF1("### DmaHandler DEF1 isnd_msg error (%ld)\n", err);
		}
	}
	else
	{
		ptr->msginfo = USBC_MSG_HCD_D0FIFO_INT;
		/* Send message */
		err = USBC_ISND_MSG(USB_HCD_MBX, (USBC_MSG_t*)ptr);
		if( err != USBC_E_OK )
		{
			USBC_PRINTF1("### DmaHandler DEF2 isnd_msg error (%ld)\n", err);
		}
	}
#endif	/* USB_FUNCSEL_PP == USBC_HOST_PERI_PP */
#endif
/* USBC_TARGET_CHIP_PP == USBC_RX600_PP || USBC_TARGET_CHIP_PP == USBC_ASSP_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/*===========================================================================*/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_597IP_PP
/******************************************************************************
Function Name   : usb_cstd_UsbHandler
Description     : USB interrupt Handler
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_UsbHandler(void)
{
	USBC_ER_t		err;
	USBC_UTR_t		*ptr;

/* Condition compilation by the difference of USB function */
#if USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP
	usb_cstd_InterruptClock();
#endif /* USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP */

	/* Initial pointer */
	ptr = &usb_gcstd_IntMsg[usb_gcstd_IntMsgCnt];
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
	/* Peripheral Interrupt handler */
	usb_pstd_InterruptHandler(&ptr->keyword, &ptr->status);
	ptr->msghead = (USBC_MH_t)USBC_NULL;
	/* Send message */
	err = USBC_ISND_MSG(USB_PCD_MBX, (USBC_MSG_t*)ptr);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("### lib_UsbHandler isnd_msg error (%ld)\n", err);
	}
#endif	/* USBC_PERI_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PP
	/* Host Interrupt handler */
	usb_hstd_InterruptHandler(&ptr->keyword, &ptr->status);
	ptr->msghead = (USBC_MH_t)USBC_NULL;
	/* Send message */
	err = USBC_ISND_MSG(USB_HCD_MBX, (USBC_MSG_t*)ptr);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("### lib_UsbHandler isnd_msg error (%ld)\n", err);
	}
#endif	/* USBC_HOST_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PERI_PP
	/* Check Host or Peripheral */
	if( usb_cstd_FunctionUsbip() == USBC_NO )
	{
		/* Peripheral Function */
		/* Peripheral Interrupt handler */
		usb_pstd_InterruptHandler(&ptr->keyword, &ptr->status);
		ptr->msghead = (USBC_MH_t)USBC_NULL;
		/* Send message */
		err = USBC_ISND_MSG(USB_PCD_MBX, (USBC_MSG_t*)ptr);
		if( err != USBC_E_OK )
		{
			USBC_PRINTF1("### lib_UsbHandler DEF1 isnd_msg error (%ld)\n"
				, err);
		}
	}
	else
	{
		/* Host Function */
		/* Host Interrupt handler */
		usb_hstd_InterruptHandler(&ptr->keyword, &ptr->status);
		ptr->msghead = (USBC_MH_t)USBC_NULL;
		/* Send message */
		err = USBC_ISND_MSG(USB_HCD_MBX, (USBC_MSG_t*)ptr);
		if( err != USBC_E_OK )
		{
			USBC_PRINTF1("### lib_UsbHandler DEF2 isnd_msg error (%ld)\n"
				, err);
		}
	}
#endif	/* USBC_HOST_PERI_PP */

	/* Renewal Message count  */
	usb_gcstd_IntMsgCnt++;
	if( usb_gcstd_IntMsgCnt == USBC_INTMSGMAX )
	{
		usb_gcstd_IntMsgCnt = 0;
	}
}
#endif /* USBC_597IP_PP */
/******************************************************************************
End of function
******************************************************************************/



/*===========================================================================*/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_596IP_PP
/******************************************************************************
Function Name   : usb_cstd_UsbHandler
Description     : USB interrupt Handler
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_UsbHandler(void)
{
	USBC_ER_t		err;
	USBC_UTR_t		*ptr;

	/* Initial pointer */
	ptr = &usb_gcstd_IntMsg[usb_gcstd_IntMsgCnt];
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
	/* Peripheral Interrupt handler */
	usb_pstd_InterruptHandler(&ptr->keyword, &ptr->status);
	ptr->msghead = (USBC_MH_t)USBC_NULL;
	/* Send message */
	err = USBC_ISND_MSG(USB_PCD_MBX, (USBC_MSG_t*)ptr);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("### lib_UsbHandler isnd_msg error (%ld)\n", err);
	}
#endif	/* USBC_PERI_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PP
	/* Host Interrupt handler */
	usb_hstd_InterruptHandler(&ptr->keyword, &ptr->status);
	ptr->msghead = (USBC_MH_t)USBC_NULL;
	/* Send message */
	err = USBC_ISND_MSG(USB_HCD_MBX, (USBC_MSG_t*)ptr);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("### lib_UsbHandler isnd_msg error (%ld)\n", err);
	}
#endif	/* USBC_HOST_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PERI_PP
	switch( usb_gcstd_PcutMode )
	{
	case USB_HOST:
		/* Host Interrupt handler */
		usb_hstd_InterruptHandler(&ptr->keyword, &ptr->status);
		ptr->msghead = (USBC_MH_t)USBC_NULL;
		/* Send message */
		err = USBC_ISND_MSG(USB_HCD_MBX, (USBC_MSG_t*)ptr);
		if( err != USBC_E_OK )
		{
			USBC_PRINTF1("### lib_UsbHandler HOST isnd_msg error (%ld)\n"
				, err);
		}
		break;
	case USBC_PERI:
		/* Peripheral Interrupt handler */
		usb_pstd_InterruptHandler(&ptr->keyword, &ptr->status);
		ptr->msghead = (USBC_MH_t)USBC_NULL;
		/* Send message */
		err = USBC_ISND_MSG(USB_PCD_MBX, (USBC_MSG_t*)ptr);
		if( err != USBC_E_OK )
		{
			USBC_PRINTF1("### lib_UsbHandler PERI isnd_msg error (%ld)\n"
				, err);
		}
		break;
	default:
		if( usb_cstd_FunctionUsbip() == USBC_NO )
		{
			/* Peripheral Function */
			/* Peripheral Interrupt handler */
			usb_pstd_InterruptHandler(&ptr->keyword, &ptr->status);
			ptr->msghead = (USBC_MH_t)USBC_NULL;
			/* Send message */
			err = USBC_ISND_MSG(USB_PCD_MBX, (USBC_MSG_t*)ptr);
			if( err != USBC_E_OK )
			{
				USBC_PRINTF1("### lib_UsbHandler DEF1 isnd_msg error (%ld)\n"
					, err);
			}
		}
		else
		{
			/* Host Interrupt handler */
			usb_hstd_InterruptHandler(&ptr->keyword, &ptr->status);
			ptr->msghead = (USBC_MH_t)USBC_NULL;
			/* Send message */
			err = USBC_ISND_MSG(USB_HCD_MBX, (USBC_MSG_t*)ptr);
			if( err != USBC_E_OK )
			{
				USBC_PRINTF1("### lib_UsbHandler DEF2 isnd_msg error (%ld)\n"
					, err);
			}
		}
		break;
	}
#endif	/* USBC_HOST_PERI_PP */

	/* Renewal Message count  */
	usb_gcstd_IntMsgCnt++;
	if( usb_gcstd_IntMsgCnt == USBC_INTMSGMAX )
	{
		usb_gcstd_IntMsgCnt = 0;
	}
}
#endif /* USBC_596IP_PP */
/******************************************************************************
End of function
******************************************************************************/



/*===========================================================================*/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_592IP_PP
/******************************************************************************
Function Name   : usb_cstd_UsbHandler
Description     : USB interrupt Handler
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_UsbHandler(void)
{
	USBC_ER_t		err;
	USBC_UTR_t		*ptr;

	/* Initial pointer */
	ptr = &usb_gcstd_IntMsg[usb_gcstd_IntMsgCnt];

	/* Peripheral Interrupt handler */
	usb_pstd_InterruptHandler(&ptr->keyword, &ptr->status);
	ptr->msghead = (USBC_MH_t)USBC_NULL;
	/* Send message */
	err = USBC_ISND_MSG(USB_PCD_MBX, (USBC_MSG_t*)ptr);
	if( err != USBC_E_OK )
	{
		USBC_PRINTF1("### lib_UsbHandler isnd_msg error (%ld)\n", err);
	}

	/* Renewal Message count  */
	usb_gcstd_IntMsgCnt++;
	if( usb_gcstd_IntMsgCnt == USBC_INTMSGMAX )
	{
		usb_gcstd_IntMsgCnt = 0;
	}
}
#endif /* USBC_592IP_PP */
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
End  Of File
******************************************************************************/
