/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_cSignal.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Host and Peripheral common Signal code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usbc_cMacSystemcall.h"	/* System call macro */
#include "r_usb_cExtern.h"			/* USB-FW global define */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _usblib


/******************************************************************************
Constant macro definitions
******************************************************************************/


/******************************************************************************
External variables and functions
******************************************************************************/


/******************************************************************************
Private global variables and functions
******************************************************************************/


/******************************************************************************
Renesas Abstracted common Signal functions
******************************************************************************/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_597IP_PP


/******************************************************************************
Function Name   : usb_cstd_SetHwFunction
Description     : Set H/W function ( Host function or Peripheral function )
Arguments       : uint16_t function		; HOST / PERI
Return value    : none
******************************************************************************/
void usb_cstd_SetHwFunction(uint16_t function)
{
	/* USB interrupt message initialize */
	usb_cstd_InitUsbMessage(function);
	/* Select HW function */
	usb_cstd_SetHwFunctionSub(function);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_cstd_ClearHwFunction
Description     : Clear H/W function
Arguments       : none
Return value    : none
******************************************************************************/
void R_usb_cstd_ClearHwFunction(void)
{
	usb_cstd_SelfClock();

	usb_cstd_SetNak( USBC_PIPE0 );

	USB_WR(INTENB0,0);
/* Condition compilation by the difference of USB function */
#if	USB_FUNCSEL_PP != USBC_PERI_PP
	USB_WR(INTENB1,0);
/* Condition compilation by the difference of the devices */
#if USBC_PORTSEL_PP == USBC_2PORT_PP
	USB_WR(INTENB2,0);
#endif	/* USBC_2PORT_PP */
#endif	/* != USBC_PERI_PP */

	/* Interrupt Disable(BRDY,NRDY,USBC_BEMP) */
	USB_WR(BRDYENB,0);
	USB_WR(NRDYENB,0);
	USB_WR(BEMPENB,0);
	/* Interrupt status clear */
	USB_WR(INTSTS0,0);
/* Condition compilation by the difference of USB function */
#if	USB_FUNCSEL_PP != USBC_PERI_PP
	USB_WR(INTSTS1,0);
/* Condition compilation by the difference of the devices */
#if USBC_PORTSEL_PP == USBC_2PORT_PP
	USB_WR(INTSTS2,0);
#endif	/* USBC_2PORT_PP */
#endif	/* != USBC_PERI_PP */


	/* Interrupt status clear(USBC_BRDY,NRDY,USBC_BEMP) */
	USB_WR(BRDYSTS,0);
	USB_WR(NRDYSTS,0);
	USB_WR(BEMPSTS,0);

	/* D+/D- control line set */
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	USB_CLR_PAT(SYSCFG, (USBC_DRPD | USBC_DPRPU));
	USB_CLR_PAT(SYSCFG, USBC_HSE);
#else	/* !USBC_RX600_PP */
	USB_CLR_PAT(SYSCFG0, (USBC_DRPD | USBC_DPRPU));
	USB_CLR_PAT(SYSCFG0, USBC_HSE);
#endif	/* USBC_RX600_PP */

/* Condition compilation by the difference of the devices */
#if USBC_PORTSEL_PP == USBC_2PORT_PP
	USB_CLR_PAT(SYSCFG1, (USBC_DRPD));
	USB_CLR_PAT(SYSCFG1, USBC_HSE);
#endif	/* USBC_2PORT_PP */

	/* Function controller select */
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	USB_CLR_PAT(SYSCFG, USBC_DCFM);
#else	/* !USBC_RX600_PP */
	USB_CLR_PAT(SYSCFG0, USBC_DCFM);
#endif	/* USBC_RX600_PP */
	usb_cstd_SwReset();

}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_AsspConfig
Description     : Set pin configuration register before started clock
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_AsspConfig(void)
{
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_ASSP_PP
	/* 3.3V -> LDRV=1 */
	USB_MDF_PAT(PINCFG, USBC_LDRVSEL, USBC_LDRV);
#endif	/* USBC_ASSP_PP */

}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_Pinconfig
Description     : Set bus interface register after started clock
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_Pinconfig(void)
{
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_ASSP_PP
/* Condition compilation by the difference of the devices */
  #if USB_LPWRSEL_PP == USBC_LPSM_ENABLE_PP
	/* Enable Low-power mode */
	USB_SET_PAT(SYSCFG1, USBC_LPSME);
	/* Enable CS return by CS assert */
	USB_CLR_PAT(SYSCFG1, USBC_PCSDIS);
  #endif	/* USBC_LPSM_ENABLE_PP */
#endif	/* USBC_ASSP_PP */
	/* CFIFO Port Select Register  (0x1E) */
	USB_WR(CFIFOSEL,  USB_CFIFO_MBW);
	/* D0FIFO Port Select Register (0x24) */
	USB_WR(D0FIFOSEL, USB_D0FIFO_MBW);
	/* D1FIFO Port Select Register (0x2A) */
	USB_WR(D1FIFOSEL, USB_D1FIFO_MBW);

	/* BIGENDIAN -> BIGEND=1 */
	USB_MDF_PAT(CFIFOSEL,  USB_FIFOENDIAN, USBC_BIGEND);
	/* BIGENDIAN -> BIGEND=1 */
	USB_MDF_PAT(D0FIFOSEL, USB_FIFOENDIAN, USBC_BIGEND);
	/* BIGENDIAN -> BIGEND=1 */
	USB_MDF_PAT(D1FIFOSEL, USB_FIFOENDIAN, USBC_BIGEND);

/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_ASSP_PP
	USB_WR(DMA0CFG, USBC_BURST|USBC_CPU_DACK_ONLY);
#endif	/* USBC_ASSP_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_InitialClock
Description     : Start Oscillation: H/W reset
                : Host : reset start, Peripheral : reset start
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_InitialClock(void)
{
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_ASSP_PP
	uint16_t	buf;

	/* External Clock Enable */
	USB_SET_PAT(SYSCFG0, USBC_XCKE);
	do
	{
		/* Wait 1ms */
		usbc_cpu_DelayXms((uint16_t)1u);
		USB_RD(SYSCFG0, buf);
	}
	while( (uint16_t)(buf & USBC_SCKE) == 0u );
#else	/* !USBC_ASSP_PP */
/* Condition compilation by the difference of the devices */
 #if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	/* SCLK Enable */
	USB_SET_PAT(SYSCFG, USBC_SCKE);
 #else	/* !USBC_RX600_PP */
	/* SCLK Enable */
	USB_SET_PAT(SYSCFG0, USBC_SCKE);
 #endif	/* USBC_RX600_PP */
#endif	/* USBC_ASSP_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_InterruptClock
Description     : Start Oscillation: Interrupt wakeup
                : Host : ATCH/DTCH/BCHG(remote),
                : Peripheral : VBINT(attach/detach)/RESM
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_InterruptClock(void)
{
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_NOT_STOP_PP
	/* Nothing */
#endif	/* USBC_CLK_NOT_STOP_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP
	uint16_t	buf;

	if( usb_gcstd_XckeMode == USBC_YES )
	{
		usb_gcstd_XckeMode = USBC_NO;
		/* Min 1ms wait */
		usbc_cpu_DelayXms(2);
		do
		{
			/* Wait 10us */
			usbc_cpu_Delay1us((uint16_t)10);
			USB_RD(SYSCFG0, buf);
		}
		while( (uint16_t)(buf & USBC_SCKE) == 0 );
		/* External Clock Enable */
		USB_SET_PAT(SYSCFG0, USBC_XCKE);
	}
#endif	/* USBC_CLK_XCKE_USE_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SelfClock
Description     : Start Oscillation: CS wakeup
                : Host : resume, Peripheral : remote wakeup
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_SelfClock(void)
{
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_NOT_STOP_PP
	/* Nothing */
#endif	/* USBC_CLK_NOT_STOP_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP
	uint16_t	buf;

	if( usb_gcstd_XckeMode == USBC_YES )
	{
		/* Dummy read */
		USB_RD(SYSCFG0, buf);
		usb_cstd_InterruptClock();
#if USB_FUNCSEL_PP == USBC_HOST_PP
		usb_hstd_BchgDisable(USBC_PORT0);
/* Condition compilation by the difference of the devices */
#if USBC_PORTSEL_PP == USBC_2PORT_PP
		usb_hstd_BchgDisable(USBC_PORT1);
#endif /* USBC_PORTSEL_PP == USBC_2PORT_PP */
#endif	/* USB_FUNCSEL_PP == USBC_HOST_PP */
	}
#endif	/* USBC_CLK_XCKE_USE_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_StopClock
Description     : Stop Clock
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_StopClock(void)
{
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_NOT_STOP_PP
	/* Nothing */
#endif	/* USBC_CLK_NOT_STOP_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP
	uint16_t	buf;

	usb_gcstd_XckeMode = USBC_YES;
	do
	{
		/* Wait 640ns */
		usbc_cpu_Delay1us((uint16_t)1);
		USB_RD(SOFCFG, buf);
	}
	while( (uint16_t)(buf & USBC_EDGESTS) == USBC_EDGESTS );

	/* SCLK disable */
	USB_CLR_PAT(SYSCFG0, USBC_SCKE);
	/* Wait 60ns/300ns */
	usbc_cpu_Delay1us((uint16_t)1);
	/* PLL control disable */
	USB_CLR_PAT(SYSCFG0, USBC_PLLC);
	/* XCK buffer disable */
	USB_CLR_PAT(SYSCFG0, USBC_XCKE);
	/* Wait 10us */
	usbc_cpu_Delay1us((uint16_t)10);
#endif	/* USBC_CLK_XCKE_USE_PP */
}
/******************************************************************************
End of function
******************************************************************************/

/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
uint16_t	usb_gcstd_RhstBit;
uint16_t	usb_gcstd_DvsqBit;
uint16_t	usb_gcstd_AddrBit;
uint16_t	usb_gcstd_SqmonBit;

/******************************************************************************
Function Name   : usb_cstd_RegBackup
Description     : Register Backup
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_RegBackup(void)
{
	uint16_t	buf;

	/* USB address */
	USB_RD(USBADDR,	 usb_gcstd_AddrBit);
	/* USB device state */
	USB_RD(INTSTS0,	 usb_gcstd_DvsqBit);
	/* USB speed */
	USB_RD(DVSTCTR0, usb_gcstd_RhstBit);

	/* SQMON bit Backup */
	usb_gcstd_SqmonBit = 0x00;
	USB_RD(PIPE1CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT1;
	}
	USB_RD(PIPE2CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT2;
	}
	USB_RD(PIPE3CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT3;
	}
	USB_RD(PIPE4CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT4;
	}
	USB_RD(PIPE5CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT5;
	}
	USB_RD(PIPE6CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT6;
	}
	USB_RD(PIPE7CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT7;
	}
	USB_RD(PIPE8CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT8;
	}
	USB_RD(PIPE9CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT9;
	}
}
#endif	/* USBC_RX600_PP */

#endif	/* USBC_597IP_PP */
/******************************************************************************
End of function
******************************************************************************/



/*===========================================================================*/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_596IP_PP


/******************************************************************************
Function Name   : usb_cstd_SetHwFunction
Description     : Set H/W function ( Host function or Peripheral function )
Arguments       : uint16_t function		; HOST / PERI
Return value    : none
******************************************************************************/
void usb_cstd_SetHwFunction(uint16_t function)
{
	/* SCLK disable */
	USB_CLR_PAT(SYSCFG0, USBC_SCKE);
	/* Wait 60ns */
	usbc_cpu_Delay1us((uint16_t)1);
	/* USB interrupt message initialize */
	usb_cstd_InitUsbMessage(function);
	/* Check Host or Peripheral */
	switch( function )
	{
	case USBC_HOST:
/* Condition compilation by the difference of user define */
#if USBC_SPEEDSEL_PP == USBC_HS_PP
		usb_cstd_SetHse(USBC_PORT0, usb_gcstd_HsEnable);
#endif	/* USBC_HS_PP */
		/* HOST mode */
		USB_SET_PAT(SYSCFG0, (USBC_DCFM | USBC_DRPD | USBC_FSRPC));
		/* Interrupt Enable */
		usb_cstd_BerneEnable();
		usb_hstd_InitConnect(USBC_PORT0);
		break;
	case USBC_PERI:
		/* Interrupt Enable */
		usb_cstd_BerneEnable();
		usb_pstd_InitConnect();
		break;
	default:
		USBC_PRINTF1("Not support function(%d)\n", function);
		break;
	}
	/* Wait 60ns */
	usbc_cpu_Delay1us((uint16_t)1);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_cstd_ClearHwFunction
Description     : Clear H/W function
Arguments       : none
Return value    : none
******************************************************************************/
void R_usb_cstd_ClearHwFunction(void)
{
	/* Clk control */
	usb_cstd_SelfClock();

	usb_cstd_SetNak( USBC_PIPE0 );

	/* Register clear */
	USB_WR(DVSTCTR0, 0);
	USB_WR(INTENB0, 0);
/* Condition compilation by the difference of USB function */
#if	USB_FUNCSEL_PP != USBC_PERI_PP
	USB_WR(INTENB1, 0);
#endif	/* != USBC_PERI_PP */
	USB_WR(BRDYENB, 0);
	USB_WR(NRDYENB, 0);
	USB_WR(BEMPENB, 0);
	USB_WR(INTSTS0, 0);
/* Condition compilation by the difference of USB function */
#if	USB_FUNCSEL_PP != USBC_PERI_PP
	USB_WR(INTSTS1, 0);
#endif	/* != USBC_PERI_PP */
	USB_WR(BRDYSTS, 0);
	USB_WR(NRDYSTS, 0);
	USB_WR(BEMPSTS, 0);
	/* SCLK disable */
	USB_CLR_PAT(SYSCFG0, USBC_SCKE);
	USB_CLR_PAT(SYSCFG0, (USBC_DRPD | USBC_DPRPU));
	USB_CLR_PAT(SYSCFG0, USBC_HSE);
	/* SCLK enable */
	USB_SET_PAT(SYSCFG0, USBC_SCKE);
	usb_cstd_SwReset();
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_AsspConfig
Description     : Set pin configuration register before started clock
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_AsspConfig(void)
{
	/* 3.3V -> LDRV=1 */
	USB_MDF_PAT(PINCFG, USBC_LDRVSEL, USBC_LDRV);
	/* BIGENDIAN -> BIGEND=1 */
	USB_MDF_PAT(PINCFG, USB_FIFOENDIAN, USBC_BIGEND);

/* Condition compilation by the difference of the devices */
#if USB_ATCKMSEL_PP == USBC_ATCKM_USE_PP
	/* Auto Clock Mode enable */
	USB_SET_PAT(SYSCFG0,  USBC_ATCKM);
#else	/* !USBC_ATCKM_USE_PP */
	/* Auto Clock Mode disable */
	USB_CLR_PAT(SYSCFG0,  USBC_ATCKM);
#endif	/* USBC_ATCKM_USE_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_Pinconfig
Description     : Set bus interface register after started clock
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_Pinconfig(void)
{
/* Condition compilation by the difference of USB function */
#if	USB_FUNCSEL_PP != USBC_PERI_PP
	/* Enable CS return by CS assert */
	USB_CLR_PAT(INTENB1, USBC_PCSE);
#endif	/* != USBC_PERI_PP */

	/* CFIFO Port Select Register  (0x1E) */
	USB_WR(CFIFOSEL,  USB_CFIFO_MBW);
	/* D0FIFO Port Select Register (0x24) */
	USB_WR(D0FIFOSEL, USB_D0FIFO_MBW);
	/* D1FIFO Port Select Register (0x2A) */
	USB_WR(D1FIFOSEL, USB_D1FIFO_MBW);

/* Condition compilation by the difference of USB function */
#if	USB_FUNCSEL_PP != USBC_PERI_PP
	/* INT level sense */
	USB_SET_PAT(INTENB1, USBC_INTL);
#endif	/* != USBC_PERI_PP */

/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_ASSP_PP
	USB_WR(DMA0CFG, USBC_BURST|USBC_CPU_DACK_ONLY);
#endif	/* USBC_ASSP_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_InitialClock
Description     : Start Oscillation : H/W reset
                : Host : reset start, Peripheral : reset start
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_InitialClock(void)
{
	uint16_t	buf;

	/* External Clock Enable */
	USB_SET_PAT(SYSCFG0, USBC_XCKE);
/* Condition compilation by the difference of the devices */
#if USB_ATCKMSEL_PP == USBC_ATCKM_USE_PP
	do
	{
		/* Wait 1ms */
		usbc_cpu_DelayXms(1);
		USB_RD(SYSCFG0, buf);
	}
	while( (uint16_t)(buf & USBC_SCKE) == 0 );
#else	/* !USBC_ATCKM_USE_PP */
	/* Wait 2ms */
	usbc_cpu_DelayXms(2);
	/* RCKE & PLLC Enable */
	USB_SET_PAT(SYSCFG0, (RCKE | USBC_PLLC));
	/* Wait 10us */
	usbc_cpu_Delay1us((uint16_t)10);
	/* SCLK Enable */
	USB_SET_PAT(SYSCFG0, USBC_SCKE);
#endif	/* USBC_ATCKM_USE_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_InterruptClock
Description     : Start Oscillation : Interrupt wakeup
                : Host : ATCH/DTCH/BCHG(remote),
                : Peripheral : VBINT(attach/detach)/RESM
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_InterruptClock(void)
{
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_NOT_STOP_PP
	/* Nothing */
#endif	/* USBC_CLK_NOT_STOP_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP
	switch( usb_gcstd_PcutMode )
	{
	case USB_HOST:
		usb_gcstd_PcutMode = USBC_NO;
		/* Wait 3ms */
		usbc_cpu_DelayXms((uint16_t)3);
		usb_cstd_AsspConfig();
		usb_cstd_Pinconfig();
		/* Interrupt Enable */
		usb_cstd_BerneEnable();
		break;
	case USBC_PERI:
		usb_gcstd_PcutMode = USBC_NO;
		/* Wait 3ms */
		usbc_cpu_DelayXms((uint16_t)3);
		usb_cstd_AsspConfig();
		usb_cstd_Pinconfig();
		/* Interrupt Enable */
		usb_cstd_BerneEnable();
		/* Interrupt Enable */
		usb_pstd_InterruptEnable();
		break;
	default:
		break;
	}
#endif	/* USBC_CLK_PCUT_USE_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP
	if( usb_gcstd_XckeMode == USBC_YES )
	{
		usb_gcstd_XckeMode = USBC_NO;
		usb_cstd_InitialClock();
	}
#endif	/* USBC_CLK_XCKE_USE_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SelfClock
Description     : Start Oscillation : CS wakeup
                : Host : resume, Peripheral : remote wakeup
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_SelfClock(void)
{
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_NOT_STOP_PP
	/* Nothing */
#endif	/* USBC_CLK_NOT_STOP_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP
	switch( usb_gcstd_PcutMode )
	{
	case USB_HOST:
		/* PCUT Mode Flag */
		usb_gcstd_PcutMode = USBC_NO;
		/* Dummy Write for CS-return from PCUT */
		USB_WR(INVALID_REG, 0);
		/* Wait 3ms */
		usbc_cpu_DelayXms((uint16_t)3);
		usb_cstd_AsspConfig();
		usb_cstd_Pinconfig();
		usb_hstd_RegRecover();
		/* Interrupt Enable */
		usb_cstd_BerneEnable();
		break;
	case USBC_PERI:
		/* PCUT Mode Flag */
		usb_gcstd_PcutMode = USBC_NO;
		/* Dummy Write for CS-return from PCUT */
		USB_WR(INVALID_REG, 0);
		/* Wait 3ms */
		usbc_cpu_DelayXms((uint16_t)3);
		usb_cstd_AsspConfig();
		usb_cstd_Pinconfig();
		usb_pstd_RegRecover();
		/* Interrupt Enable */
		usb_cstd_BerneEnable();
		/* Interrupt Enable */
		usb_pstd_InterruptEnable();
		break;
	default:
		break;
	}
#endif	/* USBC_CLK_PCUT_USE_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP
	usb_gcstd_XckeMode = USBC_NO;
	usb_cstd_InitialClock();
#endif	/* USBC_CLK_XCKE_USE_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_RestartClock
Description     : Start Oscillation : common
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_RestartClock(void)
{
	/* RCKE & PLLC Enable */
	USB_SET_PAT(SYSCFG0, (RCKE | USBC_PLLC));
	/* Wait 10us */
	usbc_cpu_Delay1us((uint16_t)10);
	/* SCLK Enable */
	USB_SET_PAT(SYSCFG0, USBC_SCKE);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_StopClock
Description     : Stop Clock
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_StopClock(void)
{
	/* SCLK disable */
	USB_CLR_PAT(SYSCFG0, USBC_SCKE);
	/* Wait 60ns/300ns */
	usbc_cpu_Delay1us((uint16_t)1);
	/* PLL control disable */
	USB_CLR_PAT(SYSCFG0, USBC_PLLC);
	/* XCK buffer disable */
	USB_CLR_PAT(SYSCFG0, RCKE);
	/* Wait 300ns */
	usbc_cpu_Delay1us((uint16_t)1);
}
/******************************************************************************
End of function
******************************************************************************/

/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP
uint16_t	usb_gcstd_RhstBit;
uint16_t	usb_gcstd_DvsqBit;
uint16_t	usb_gcstd_AddrBit;
uint16_t	usb_gcstd_SqmonBit;

/******************************************************************************
Function Name   : usb_cstd_RegBackup
Description     : Register Backup
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_RegBackup(void)
{
	uint16_t	buf;

	/* USB address */
	USB_RD(USBADDR,	 usb_gcstd_AddrBit);
	/* USB device state */
	USB_RD(INTSTS0,	 usb_gcstd_DvsqBit);
	/* USB speed */
	USB_RD(DVSTCTR0, usb_gcstd_RhstBit);

	/* SQMON bit Backup */
	usb_gcstd_SqmonBit = 0x00;
	USB_RD(PIPE1CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT1; 
	}
	USB_RD(PIPE2CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT2; 
	}
	USB_RD(PIPE3CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT3; 
	}
	USB_RD(PIPE4CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT4; 
	}
	USB_RD(PIPE5CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT5; 
	}
	USB_RD(PIPE6CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT6; 
	}
	USB_RD(PIPE7CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT7; 
	}
	USB_RD(PIPE8CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT8; 
	}
	USB_RD(PIPE9CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT9; 
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_hstd_RegRecover
Description     : Register Recover
Arguments       : none
Return value    : none
******************************************************************************/
void usb_hstd_RegRecover(void)
{
	uint16_t		buf, md;
	USBC_HCDREG_t	*driver;
	uint16_t		*table;

	/* USB device state */
	buf = (uint16_t)((uint16_t)(usb_gcstd_DvsqBit & USBC_DVSQS) << 4);
	if( (usb_gcstd_RhstBit & USBC_RHST) == USBC_HSMODE )
	{
		/* USB speed */
		buf |= STSR_HI;
	}
	else
	{
		buf |= STSR_FL;
	}
	USB_WR(USBADDR, buf);

	/* Search device state from device address */
	if( usb_hstd_MfrMode1(USBC_DEVICEADDR) == USBC_CONFIGURED )
	{
		for( md = 0; md <u sb_hDeviceNum; md++ )
		{
			if( usb_ghstd_DeviceDrv[md].ifclass != USBC_IFCLS_NOT )
			{
				driver = (USBC_HCDREG_t*)&usb_ghstd_DeviceDrv[md];
				table = (uint16_t*)(driver->pipetbl);
				/* Set pipe configuration register */
				R_usb_pstd_SetPipeRegister(USBC_USEPIPE, table);
			}
		}
	}

	/* SQMON bit Recover */
	if( (usb_gcstd_SqmonBit & USBC_BIT1) != 0 )
	{
		USB_WR(PIPE1CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT2) != 0 )
	{
		USB_WR(PIPE2CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT3) != 0 )
	{
		USB_WR(PIPE3CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT4) != 0 )
	{
		USB_WR(PIPE4CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT5) != 0 )
	{
		USB_WR(PIPE5CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT6) != 0 )
	{
		USB_WR(PIPE6CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT7) != 0 )
	{
		USB_WR(PIPE7CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT8) != 0 )
	{
		USB_WR(PIPE8CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT9) != 0 )
	{
		USB_WR(PIPE9CTR, USBC_SQSET);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_RegRecover
Description     : Register Recover
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_RegRecover(void)
{
	uint16_t		buf;
	uint16_t*		table;

	if( (usb_gcstd_DvsqBit & USBC_DVSQS) != USBC_DS_POWR )
	{
		/* USB address */
		buf	 = usb_gcstd_AddrBit & USBC_USBADDR_MASK;
		/* USB device state */
		buf	|= ((usb_gcstd_DvsqBit & USBC_DVSQS) << 4);
		/* USB speed */
		if( (usb_gcstd_RhstBit & USBC_RHST) == USBC_HSMODE )
			buf |= STSR_HI;
		USB_WR(USBADDR, buf);
	}

	if( usb_gpstd_ConfigNum != 0 )
	{
		table = (uint16_t*)((uint16_t**)
			(usb_gpstd_Driver.pipetbl[usb_gpstd_ConfigNum - 1]));
		/* Set pipe configuration register */
		R_usb_pstd_SetPipeRegister(USBC_PERIPIPE, table);
	}

	/* SQMON bit Recover */
	if( (usb_gcstd_SqmonBit & USBC_BIT1) != 0 )
	{
		USB_WR(PIPE1CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT2) != 0 )
	{
		USB_WR(PIPE2CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT3) != 0 )
	{
		USB_WR(PIPE3CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT4) != 0 )
	{
		USB_WR(PIPE4CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT5) != 0 )
	{
		USB_WR(PIPE5CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT6) != 0 )
	{
		USB_WR(PIPE6CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT7) != 0 )
	{
		USB_WR(PIPE7CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT8) != 0 )
	{
		USB_WR(PIPE8CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT9) != 0 )
	{
		USB_WR(PIPE9CTR, USBC_SQSET);
	}
}
#endif	/* USBC_CLK_PCUT_USE_PP */

#endif	/* == USBC_596IP_PP */
/******************************************************************************
End of function
******************************************************************************/



/*===========================================================================*/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_592IP_PP


/******************************************************************************
Function Name   : usb_cstd_SetHwFunction
Description     : Set H/W function ( Host function or Peripheral function )
Arguments       : uint16_t function		; HOST / PERI
Return value    : none
******************************************************************************/
void usb_cstd_SetHwFunction(uint16_t function)
{
	/* SCLK disable */
	USB_CLR_PAT(SYSCFG0, USBC_SCKE);
	/* Wait 60ns */
	usbc_cpu_Delay1us((uint16_t)1);
	/* USB interrupt message initialize */
	usb_cstd_InitUsbMessage(function);
	/* Interrupt Enable */
	usb_cstd_BerneEnable();
	usb_pstd_InitConnect();
	/* Wait 60ns */
	usbc_cpu_Delay1us((uint16_t)1);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_cstd_ClearHwFunction
Description     : Clear H/W function
Arguments       : none
Return value    : none
******************************************************************************/
void R_usb_cstd_ClearHwFunction(void)
{
	/* Clk control */
	usb_cstd_SelfClock();

	usb_cstd_SetNak( USBC_PIPE0 );

	/* Register clear */
	USB_WR(DVSTCTR0, 0);
	USB_WR(INTENB0, 0);
/* Condition compilation by the difference of USB function */
#if	USB_FUNCSEL_PP != USBC_PERI_PP
	USB_WR(INTENB1, 0);
#endif	/* != USBC_PERI_PP */
	USB_WR(BRDYENB, 0);
	USB_WR(NRDYENB, 0);
	USB_WR(BEMPENB, 0);
	USB_WR(INTSTS0, 0);
	USB_WR(BRDYSTS, 0);
	USB_WR(NRDYSTS, 0);
	USB_WR(BEMPSTS, 0);
	/* SCLK disable */
	USB_CLR_PAT(SYSCFG0, USBC_SCKE);
	USB_CLR_PAT(SYSCFG0, USBC_DPRPU);
	USB_CLR_PAT(SYSCFG0, USBC_HSE);
	/* SCLK enable */
	USB_SET_PAT(SYSCFG0, USBC_SCKE);
	usb_cstd_SwReset();
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_AsspConfig
Description     : Set pin configuration register before started clock
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_AsspConfig(void)
{
	/* 3.3V -> LDRV=1 */
	USB_MDF_PAT(PINCFG, USB_LDRVSEL, USBC_LDRV);
	/* BIGENDIAN -> BIGEND=1 */
	USB_MDF_PAT(PINCFG, USB_FIFOENDIAN, USBC_BIGEND);
/* Condition compilation by the difference of the devices */
#if USB_ATCKMSEL_PP == USBC_ATCKM_USE_PP
	/* Auto Clock Mode enable */
	USB_SET_PAT(SYSCFG0,  ATCKM);
#else	/* !USBC_ATCKM_USE_PP */
	/* Auto Clock Mode disable */
	USB_CLR_PAT(SYSCFG0,  ATCKM);
#endif	/* USBC_ATCKM_USE_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_Pinconfig
Description     : Set bus interface register after started clock
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_Pinconfig(void)
{
	/* Enable CS return by CS assert */
/* Condition compilation by the difference of USB function */
#if	USB_FUNCSEL_PP != USBC_PERI_PP
	USB_CLR_PAT(INTENB1, PCSE);
#endif	/* != USBC_PERI_PP */

	/* CFIFO Port Select Register  (0x1E) */
	USB_WR(CFIFOSEL,  USB_CFIFO_MBW);
	/* D0FIFO Port Select Register (0x24) */
	USB_WR(D0FIFOSEL, USB_D0FIFO_MBW);
	/* D1FIFO Port Select Register (0x2A) */
	USB_WR(D1FIFOSEL, USB_D1FIFO_MBW);
/* Condition compilation by the difference of USB function */
#if	USB2_FUNCSEL_PP != USBC_PERI_PP
	/* INT level sense */
	USB_SET_PAT(INTENB1, USBC_INTL);
#endif	/* != USBC_PERI_PP */

/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_ASSP_PP
	USB_WR(DMA0CFG, USBC_BURST|USBC_CPU_DACK_ONLY);
#endif	/* USBC_ASSP_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_InitialClock
Description     : Start Oscillation : H/W reset
                : Host : reset start, Peripheral : reset start
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_InitialClock(void)
{
	uint16_t	buf;

	/* External Clock Enable */
	USB_SET_PAT(SYSCFG0, USBC_XCKE);
/* Condition compilation by the difference of the devices */
#if USB_ATCKMSEL_PP == USBC_ATCKM_USE_PP
	do
	{
		/* Wait 1ms */
		usbc_cpu_DelayXms(1);
		USB_RD(SYSCFG0, buf);
	}
	while( (uint16_t)(buf & USBC_SCKE) == 0 );
#else	/* !USBC_ATCKM_USE_PP */
	/* Wait 2ms */
	usbc_cpu_DelayXms(2);
	/* RCKE & PLLC Enable */
	USB_SET_PAT(SYSCFG0, (RCKE | PLLC));
	/* Wait 10us */
	usbc_cpu_Delay1us((uint16_t)10);
	/* SCLK Enable */
	USB_SET_PAT(SYSCFG0, USBC_SCKE);
#endif	/* USBC_ATCKM_USE_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_InterruptClock
Description     : Start Oscillation : Interrupt wakeup
                : Host : ATCH/DTCH/BCHG(remote),
                : Peripheral : VBINT(attach/detach)/RESM
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_InterruptClock(void)
{
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_NOT_STOP_PP
	/* Nothing */
#endif	/* USBC_CLK_NOT_STOP_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP
	switch( usb_gcstd_PcutMode )
	{
	case USB_HOST:
		usb_gcstd_PcutMode = USBC_NO;
		/* Wait 3ms */
		usbc_cpu_DelayXms((uint16_t)3);
		usb_cstd_AsspConfig();
		usb_cstd_Pinconfig();
		/* Interrupt Enable */
		usb_cstd_BerneEnable();
		break;
	case USBC_PERI:
		usb_gcstd_PcutMode = USBC_NO;
		/* Wait 3ms */
		usbc_cpu_DelayXms((uint16_t)3);
		usb_cstd_AsspConfig();
		usb_cstd_Pinconfig();
		/* Interrupt Enable */
		usb_cstd_BerneEnable();
		/* Interrupt Enable */
		usb_pstd_InterruptEnable();
		break;
	default:
		break;
	}
#endif	/* USBC_CLK_PCUT_USE_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP
	if( usb_gcstd_XckeMode == USBC_YES )
	{
		usb_gcstd_XckeMode = USBC_NO;
		usb_cstd_InitialClock();
	}
#endif	/* USBC_CLK_XCKE_USE_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SelfClock
Description     : Start Oscillation : CS wakeup
                : Host : resume, Peripheral : remote wakeup
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_SelfClock(void)
{
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_NOT_STOP_PP
	/* Nothing */
#endif	/* USBC_CLK_NOT_STOP_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP
	switch( usb_gcstd_PcutMode )
	{
	case USB_HOST:
		/* PCUT Mode Flag */
		usb_gcstd_PcutMode = USBC_NO;
		/* Dummy Write for CS-return from PCUT */
		USB_WR(INVALID_REG, 0);
		/* Wait 3ms */
		usbc_cpu_DelayXms((uint16_t)3);
		usb_cstd_AsspConfig();
		usb_cstd_Pinconfig();
		usb_hstd_RegRecover();
		/* Interrupt Enable */
		usb_cstd_BerneEnable();
		break;
	case USBC_PERI:
		/* PCUT Mode Flag */
		usb_gcstd_PcutMode = USBC_NO;
		/* Dummy Write for CS-return from PCUT */
		USB_WR(INVALID_REG, 0);
		/* Wait 3ms */
		usbc_cpu_DelayXms((uint16_t)3);
		usb_cstd_AsspConfig();
		usb_cstd_Pinconfig();
		usb_pstd_RegRecover();
		/* Interrupt Enable */
		usb_cstd_BerneEnable();
		/* Interrupt Enable */
		usb_pstd_InterruptEnable();
		break;
	default:
		break;
	}
#endif	/* USBC_CLK_PCUT_USE_PP */
/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_XCKE_USE_PP
	usb_gcstd_XckeMode = USBC_NO;
	usb_cstd_InitialClock();
#endif	/* USBC_CLK_XCKE_USE_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_RestartClock
Description     : Start Oscillation : common
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_RestartClock(void)
{
	/* RCKE & PLLC Enable */
	USB_SET_PAT(SYSCFG0, (RCKE | PLLC));
	/* Wait 10us */
	usbc_cpu_Delay1us((uint16_t)10);
	/* SCLK Enable */
	USB_SET_PAT(SYSCFG0, USBC_SCKE);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_StopClock
Description     : Stop Clock
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_StopClock(void)
{
	/* SCLK disable */
	USB_CLR_PAT(SYSCFG0, USBC_SCKE);
	/* Wait 60ns/300ns */
	usbc_cpu_Delay1us((uint16_t)1);
	/* PLL control disable */
	USB_CLR_PAT(SYSCFG0, PLLC);
	/* XCK buffer disable */
	USB_CLR_PAT(SYSCFG0, RCKE);
	/* Wait 300ns */
	usbc_cpu_Delay1us((uint16_t)1);
}
/******************************************************************************
End of function
******************************************************************************/

/* Condition compilation by the difference of the devices */
#if USB_OSCSEL_PP == USBC_CLK_PCUT_USE_PP
uint16_t	usb_gcstd_RhstBit;
uint16_t	usb_gcstd_DvsqBit;
uint16_t	usb_gcstd_AddrBit;
uint16_t	usb_gcstd_SqmonBit;

/******************************************************************************
Function Name   : usb_cstd_RegBackup
Description     : Register Backup
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_RegBackup(void)
{
	uint16_t	buf;

	/* USB address */
	USB_RD(USBADDR,	 usb_gcstd_AddrBit);
	/* USB device state */
	USB_RD(INTSTS0,	 usb_gcstd_DvsqBit);
	/* USB speed */
	USB_RD(DVSTCTR0, usb_gcstd_RhstBit);

	/* SQMON bit Backup */
	usb_gcstd_SqmonBit = 0x00;
	USB_RD(PIPE1CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT1;
	}
	USB_RD(PIPE2CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT2;
	}
	USB_RD(PIPE3CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT3;
	}
	USB_RD(PIPE4CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT4;
	}
	USB_RD(PIPE5CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT5;
	}
	USB_RD(PIPE6CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT6;
	}
	USB_RD(PIPE7CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT7;
	}
	USB_RD(PIPE8CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT8;
	}
	USB_RD(PIPE9CTR, buf);
	if( (buf & USBC_SQMON) == USBC_SQMON )
	{
		usb_gcstd_SqmonBit |= USBC_BIT9;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_hstd_RegRecover
Description     : Register Recover
Arguments       : none
Return value    : none
******************************************************************************/
void usb_hstd_RegRecover(void)
{
	uint16_t		buf, md;
	USBC_HCDREG_t	*driver;
	uint16_t		*table;

	/* USB device state */
	buf = (uint16_t)((uint16_t)(usb_gcstd_DvsqBit & USBC_DVSQS) << 4);
	/* Check speed */
	if( (usb_gcstd_RhstBit & USBC_RHST) == USBC_HSMODE )
	{
		/* USB speed */
		buf |= STSR_HI;
	}
	else
	{
		buf |= STSR_FL;
	}
	USB_WR(USBADDR, buf);

	/* Search device state from device address */
	if( usb_hstd_MfrMode1(USBC_DEVICEADDR) == USBC_CONFIGURED )
	{
		for( md = 0; md <u sb_hDeviceNum; md++ )
		{
			if( usb_ghstd_DeviceDrv[md].ifclass != USBC_IFCLS_NOT )
			{
				driver = (USBC_HCDREG_t*)&usb_ghstd_DeviceDrv[md];
				table = (uint16_t*)(driver->pipetbl);
				/* Set pipe configuration register */
				R_usb_pstd_SetPipeRegister(USBC_USEPIPE, table);
			}
		}
	}

	/* SQMON bit Recover */
	if( (usb_gcstd_SqmonBit & USBC_BIT1) != 0 )
	{
		USB_WR(PIPE1CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT2) != 0 )
	{
		USB_WR(PIPE2CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT3) != 0 )
	{
		USB_WR(PIPE3CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT4) != 0 )
	{
		USB_WR(PIPE4CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT5) != 0 )
	{
		USB_WR(PIPE5CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT6) != 0 )
	{
		USB_WR(PIPE6CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT7) != 0 )
	{
		USB_WR(PIPE7CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT8) != 0 )
	{
		USB_WR(PIPE8CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT9) != 0 )
	{
		USB_WR(PIPE9CTR, USBC_SQSET);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_RegRecover
Description     : Register Recover
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_RegRecover(void)
{
	uint16_t		buf;
	uint16_t*		table;

	if( (usb_gcstd_DvsqBit & USBC_DVSQS) != USBC_DS_POWR )
	{
		/* USB address */
		buf	 = usb_gcstd_AddrBit & USBC_USBADDR_MASK;
		/* USB device state */
		buf	|= ((usb_gcstd_DvsqBit & USBC_DVSQS) << 4);
		/* USB speed */
		if( (usb_gcstd_RhstBit & USBC_RHST) == USBC_HSMODE )
			buf |= STSR_HI;
		USB_WR(USBADDR, buf);
	}

	if( usb_gpstd_ConfigNum != 0 )
	{
		table = (uint16_t*)((uint16_t**)
			(usb_gpstd_Driver.pipetbl[usb_gpstd_ConfigNum - 1]));
		/* Set pipe configuration register */
		R_usb_pstd_SetPipeRegister(USBC_PERIPIPE, table);
	}

	/* SQMON bit Recover */
	if( (usb_gcstd_SqmonBit & USBC_BIT1) != 0 )
	{
		USB_WR(PIPE1CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT2) != 0 )
	{
		USB_WR(PIPE2CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT3) != 0 )
	{
		USB_WR(PIPE3CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT4) != 0 )
	{
		USB_WR(PIPE4CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT5) != 0 )
	{
		USB_WR(PIPE5CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT6) != 0 )
	{
		USB_WR(PIPE6CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT7) != 0 )
	{
		USB_WR(PIPE7CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT8) != 0 )
	{
		USB_WR(PIPE8CTR, USBC_SQSET);
	}
	if( (usb_gcstd_SqmonBit & USBC_BIT9) != 0 )
	{
		USB_WR(PIPE9CTR, USBC_SQSET);
	}
}
#endif	/* USBC_CLK_PCUT_USE_PP */


#endif	/* == USBC_592IP_PP */
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
End  Of File
******************************************************************************/
