/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_cStdFunction.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Host and Peripheral Standard Function code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usbc_cMacSystemcall.h"	/* System call macro */
#include "r_usb_cExtern.h"			/* USB-FW global define */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _usblib


/******************************************************************************
Constant macro definitions
******************************************************************************/


/******************************************************************************
External variables and functions
******************************************************************************/


/******************************************************************************
Private global variables and functions
******************************************************************************/


/******************************************************************************
Renesas Abstracted common standard function functions
******************************************************************************/

/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_597IP_PP

/* LSIG */
/******************************************************************************
Function Name   : usb_cstd_SetHwFunctionSub
Description     : Select HW function
Arguments       : uint16_t function         : Not use
Return value    : none
******************************************************************************/
void usb_cstd_SetHwFunctionSub(uint16_t function)
{
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PERI_PP
	if (function == (uint16_t)USBC_PERI)
	{
		/* Interrupt Enable */
		usb_cstd_BerneEnable();
		usb_pstd_InitConnect();
	}
	else if (function == (uint16_t)USBC_HOST)
	{
 #if USBC_TARGET_CHIP_PP == USBC_RX600_PP
		/* HOST mode */
		USB_SET_PAT(SYSCFG, (USBC_DCFM | USBC_DRPD));
 #else	/* !USBC_RX600_PP */
		/* HOST mode */
		USB_SET_PAT(SYSCFG0, (USBC_DCFM | USBC_DRPD));
 #endif	/* USBC_RX600_PP */
		/* Interrupt Enable */
		usb_cstd_BerneEnable();
		/* Wait 10us */
		usbc_cpu_Delay1us((uint16_t)10);
		usb_hstd_InitConnect(USBC_PORT0);
	}
	else
	{
	}
#endif	/* USBC_HOST_PERI_PP */

/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
	/* Interrupt Enable */
	usb_cstd_BerneEnable();
	usb_pstd_InitConnect();
#endif	/* USBC_PERI_PP */

/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PP
/* Condition compilation by the difference of the devices */
#if USBC_PORTSEL_PP == USBC_1PORT_PP
/* Condition compilation by the difference of the devices */
 #if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	/* HOST mode */
	USB_SET_PAT(SYSCFG, (USBC_DCFM | USBC_DRPD));
 #else	/* !USBC_RX600_PP */
	/* HOST mode */
	USB_SET_PAT(SYSCFG0, (USBC_DCFM | USBC_DRPD));
 #endif	/* USBC_RX600_PP */
	/* Interrupt Enable */
	usb_cstd_BerneEnable();
	/* Wait 10us */
	usbc_cpu_Delay1us((uint16_t)10);
	usb_hstd_InitConnect(USBC_PORT0);
#endif	/* USBC_1PORT_PP */
/* Condition compilation by the difference of the devices */
#if USBC_PORTSEL_PP == USBC_2PORT_PP
	/* HOST mode */
	USB_SET_PAT(SYSCFG0, (USBC_DCFM | USBC_DRPD));
	USB_SET_PAT(SYSCFG1, (USBC_DRPD));
	/* Interrupt Enable */
	usb_cstd_BerneEnable();
	/* Wait 10us */
	usbc_cpu_Delay1us((uint16_t)10);
	usb_hstd_InitConnect((uint16_t)USBC_PORT0);
	usb_hstd_InitConnect((uint16_t)USBC_PORT1);
#endif	/* USBC_2PORT_PP */

#endif	/* USBC_HOST_PP */
}

#endif	/* == USBC_597IP_PP */
/******************************************************************************
End of function
******************************************************************************/



/*===========================================================================*/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_596IP_PP

/* LSIG */
/******************************************************************************
Function Name   : usb_cstd_SetHwFunctionSub
Description     : Select HW function
Arguments       : uint16_t function         : Not use
Return value    : none
******************************************************************************/
void usb_cstd_SetHwFunctionSub(uint16_t function)
{
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
	/* Interrupt Enable */
	usb_cstd_BerneEnable();
	usb_pstd_InitConnect();
#endif	/* USBC_PERI_PP */

/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PP
	/* HOST mode */
	USB_SET_PAT(SYSCFG0, (USBC_DCFM | USBC_DRPD));
	/* Interrupt Enable */
	usb_cstd_BerneEnable();
	/* Wait 10us */
	usbc_cpu_Delay1us((uint16_t)10);
	usb_hstd_InitConnect(USBC_PORT0);
#endif	/* USBC_HOST_PP */
}

#endif	/* == USBC_596IP_PP */
/******************************************************************************
End of function
******************************************************************************/



/*===========================================================================*/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_592IP_PP

/* LSIG */
/******************************************************************************
Function Name   : usb_cstd_SetHwFunctionSub
Description     : Select HW function
Arguments       : uint16_t function         : Not use
Return value    : none
******************************************************************************/
void usb_cstd_SetHwFunctionSub(uint16_t function)
{
	/* Interrupt Enable */
	usb_cstd_BerneEnable();
	usb_pstd_InitConnect();
}

#endif	/* == USBC_592IP_PP */
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
End  Of File
******************************************************************************/
